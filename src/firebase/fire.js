import firebase from 'firebase/app'
import config from './config'
require('firebase/auth')
require('firebase/firestore')
require('firebase/storage')

const app = firebase.initializeApp(config)

const storage = firebase.storage()

const db = firebase.firestore()

export { db, storage, app as default }
