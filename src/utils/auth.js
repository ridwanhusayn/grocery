import React, { useEffect, useState } from 'react'
import app from '../firebase/fire'

export const AuthContext = React.createContext()

export const AuthProvider = ({ children }) => {
    const [currentUser, setCurrentUser] = useState(null)

    useEffect(() => {
        app.auth().onAuthStateChanged(setCurrentUser)
        return function cleanup() {
            console.log('unmounting auth');
        }
    }, [])

    return (
        <AuthContext.Provider
            value={{ currentUser }}
        >
            {children}
        </AuthContext.Provider>
    )
}
