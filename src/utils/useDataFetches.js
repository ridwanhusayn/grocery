import { useState, useEffect } from 'react'
import { db } from '../firebase/fire'
require('firebase/firestore')

const useDataFetches = (collection) => {

    const [response, setResponse] = useState(null)
    const [error, setError] = useState(null)
    const [isLoading, setIsLoading] = useState(null)

    useEffect(() => {
        const fetchData = async () => {
            setIsLoading(1)
            setError(false)
            try {
                await db.collection(collection)
                    .onSnapshot(data => {
                        setResponse(data.docs.map(doc => ({ ...doc.data(), id: doc.id })))
                        setIsLoading(0)
                        console.log(collection + ' data fetched')
                    })

            } catch (error) {
                setError(error)
                console.log(error);
            }
        }
        fetchData()
        return function cleanup() {
            console.log('cleaned up')
        };
    }, [])

    return [response, error, isLoading]
}

export default useDataFetches


