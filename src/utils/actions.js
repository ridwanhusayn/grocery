import { db } from "../firebase/fire";
import app from "../firebase/fire";
const firebase = require("firebase");
require("firebase/firestore");


export function addDisplayName() {
    firebase.auth().createUserWithEmailAndPassword('m.siddeka@gmail.com', 'Maryam123')
        .then(function (result) {
            return result.user.updateProfile({
                displayName: "Momina Husayn"
            })
        }).catch(function (error) {
            console.log(error);
        });
}

export function availableItems(items) {
    return items.filter(row => row.level !== "Out")
}

export function uniqueCategories(items) {
    if (items) {
        let categories = items.map((el) => {
            return el.category;
        }, []);
        const uc = [...new Set(categories)];
        return uc
    }
}

export function editHandler(itemData) {
    return db.collection("items").doc(itemData.id).update(itemData)
};

export function updateLevel(id, field, fieldData) {
    return db.collection("items").doc(id).update(field, fieldData)
};

export function updateChecked(listId, id, field, fieldData) {
    return db.collection('lists').doc(listId)
        .collection('items').doc(id)
        .update(field, fieldData)
};

export function getSpecificItems(field, value) {
    const keyValue = '=='
    const arrayC = 'array-contains'
    const operator = field === 'tags' ? arrayC : keyValue

    return db.collection("items").where(field, operator, value).get()
};

export function removeFieldValueItems(items, field, fieldValue) {
    const batch = db.batch();

    items.forEach((item) => {
        const laRef = db.collection("items").doc(item.id);
        if (field === "category") {
            batch.update(laRef, { "category": "" });
        }
        if (field === "storage") {
            batch.update(laRef, { "storage": "" });
        }
        if (field === "tags") {
            batch.update(laRef, { tags: firebase.firestore.FieldValue.arrayRemove(fieldValue) });
        }
    });
    return batch.commit()
};

export function editGroupHandler(itemData, page) {
    let collectionName = page.toLowerCase()
    return db.collection(collectionName)
        .doc(itemData.id)
        .update(itemData)
};

export function addHandler(itemData) {
    return db.collection("items")
        .add(itemData)
};

export function addGroupHandler(itemData, page) {
    let collectionName = page.toLowerCase()
    return db.collection(collectionName)
        .add(itemData)
};

export function addListHandler(itemData, page) {
    let collectionName = page.toLowerCase()
    return db.collection(collectionName)
        .add(itemData)
};

export function addItemSC(items, selectedList) {

    const batch = db.batch();
    items.forEach((item) => {
        const laRef = db.collection('lists').doc(selectedList).collection('items').doc();
        batch.set(laRef, item);
    });
    return batch.commit()

};

export function getCurrentDate(separator = '-') {

    let newDate = new Date();
    let date = newDate.getDate();
    let month = newDate.getMonth() + 1;
    let year = newDate.getFullYear();
    let hours = newDate.getHours();
    let minutes = newDate.getMinutes();

    return `${date < 10 ? `0${date}` : `${date}`} ${separator} ${month < 10 ? `0${month}` : `${month}`} ${separator} ${year} ${hours < 10 ? `0${hours}` : `${hours}`}: ${minutes < 10 ? `0${minutes}` : `${minutes}`} `
};

export function convertedDate(setDate) {

    let separator = '-'
    let dd = setDate.toDate()
    // let dd = setDate
    let date = dd.getDate();
    let month = dd.getMonth() + 1;
    let year = dd.getFullYear();

    return `${date < 10 ? `0${date}` : `${date}`}${separator}${month < 10 ? `0${month}` : `${month}`}${separator}${year} `

}

export function forDateDns(date) {
    let dd = date.toDate()
    return dd
}

export function deleteHandler(ids) {
    const batch = db.batch();
    ids.forEach((id) => {
        const laRef = db.collection("items").doc(id);
        batch.delete(laRef);
    });
    return batch.commit()
};

export function deleteSingleHandler(id) {
    return db.collection("items").doc(id).delete()
};

export function deleteCheckHandler(listId, id) {
    return db.collection('lists').doc(listId)
        .collection('items').doc(id)
        .delete()
};

export function deletegroupHandler(id, page) {
    let collectionName = page.toLowerCase()
    return db.collection(collectionName).doc(id)
        .delete()
};

export function filterArray(array, filters) {
    const filterKeys = Object.keys(filters);
    return array.filter((item) => {
        return filterKeys.every((key) => {
            if (typeof filters[key] !== "function") return true;
            return filters[key](item[key]);
        });
    });
}

export function fiterFunctionCreator(checklist) {
    let filters = {}

    if (checklist.categories.length > 0) {
        filters.category = (category) => checklist.categories.includes(category)
    }
    if (checklist.levels.length > 0) {
        filters.level = (level) => checklist.levels.includes(level)
    }
    if (checklist.storages.length > 0) {
        filters.storage = (storage) => checklist.storages.includes(storage)
    }
    if (checklist.tags.length > 0) {
        filters.tags = (tags) => tags && tags.some((eachTag) => checklist.tags.includes(eachTag))
    }

    return filters
}

export function userLogout() {
    try {
        app.auth().signOut();
        console.log('Logged Out')
    } catch (error) {
        alert(error);
        console.log(error);
    }
}

export function search(searchT, display) {
    let searched = null

    if (searchT) {
        searched = display.filter((data) => {
            return data.name.toLowerCase().indexOf(searchT.toLowerCase()) !== -1;
        });
        return searched
    }
}

export function compare(a, b) {
    const bandA = a.name.toUpperCase();
    const bandB = b.name.toUpperCase();

    let comparison = 0;
    if (bandA > bandB) {
        comparison = 1;
    } else if (bandA < bandB) {
        comparison = -1;
    }
    return comparison;
}
