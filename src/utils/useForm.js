import React, { useState } from 'react'
import { makeStyles } from "@material-ui/core";

export function useForm(initialFValues, validateOnChange = false, validate) {


    const [values, setValues] = useState(initialFValues);
    const [errors, setErrors] = useState({});
    const [selectedDate, handleDateChange] = useState(null);
    const [changeMade, setchangeMade] = useState(false)

    const handleInputChange = e => {
        if (!changeMade) {
            setchangeMade(true)
        }
        const { name, value } = e.target
        console.log(name, value);
        setValues({
            ...values,
            [name]: value
        })
        if (validateOnChange)
            validate({ [name]: value })
    }

    const exDate = (date) => {
        handleDateChange(date)
        if (!changeMade) {
            setchangeMade(true)
        }

    }

    const resetForm = () => {
        setValues(initialFValues);
        setErrors({})
    }


    return {
        values,
        setValues,
        errors,
        setErrors,
        handleInputChange,
        resetForm,
        changeMade,
        setchangeMade,
        selectedDate,
        handleDateChange,
        exDate

    }
}


export function Form(props) {

    const { children, ...other } = props;
    return (
        <form
            autoComplete="off" {...other}>
            {props.children}
        </form>
    )
}
