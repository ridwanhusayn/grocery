import { useState, useEffect } from 'react'
import { db } from '../firebase/fire'
require('firebase/firestore')

const useDataFetchWhere = (collection, doc, subCollection) => {

    const [response, setResponse] = useState(null)
    const [error, setError] = useState(null)
    const [isLoading, setIsLoading] = useState(null)


    useEffect(() => {
        const fetchData = async () => {
            setIsLoading(1)
            setError(false)
            try {
                let items = []
                await db.collection("lists").doc(doc).collection("items").get()
                    .then(querySnapshot => {
                        querySnapshot.forEach(doc => {
                            items.push({
                                ...doc.data(), docId: doc.id,
                            })
                        })
                        console.log(items);
                    })
                setResponse(items)
                setIsLoading(0)

            } catch (error) {
                setError(error)
            }
        }
        fetchData()
        return function cleanup() {
            console.log('cleaned up where')
        };
    }, [doc])

    return [response, error, isLoading]
}

export default useDataFetchWhere


