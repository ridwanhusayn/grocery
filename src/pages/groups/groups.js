import React, { Fragment, useEffect, useState } from "react";
import { AppState } from '../../context/app-context'
// import useDataFetches from "../utils/useDataFetches";
import { compare } from '../../utils/actions';
import EditGroups from "../../components/modals/editGroups";
import DeleteGroups from "../../components/modals/deleteGroups";
import BottomBar from "../../components/bottomBar/bottomBar";
import Navbar from "../../components/navbar/navbar";

import {
    Grid, ListItem, ListItemAvatar, Avatar, ListItemText, Divider, List,
    IconButton, ListItemSecondaryAction, Checkbox, makeStyles,
} from "@material-ui/core";
import { Delete, Edit } from "@material-ui/icons/";


const useStyles = makeStyles((theme) => ({
    catColor: {
        backgroundColor: "#e4e4e4",
    },
    pageBottom: {
        paddingBottom: theme.spacing(7),
        paddingTop: "0px"
    },
    iconPad: {
        paddingRight: "5px"
    }
}));


export default function Groups() {

    const classes = useStyles();
    const [
        stateContext, dispatch
    ] = AppState()
    const { allItems, allCategories, allStorage, allItemTags, page } = stateContext;

    // const [response1, error1, isLoading1] = useDataFetches("category");
    // const [response2, error2, isLoading2] = useDataFetches("storage");
    // const [response4, error4, isLoading4] = useDataFetches("tags");

    const [groupData, setgroupData] = useState([])
    const [IndData, setIndData] = useState([]);
    const [dialogOpen, setDialogOpen] = useState(false);
    const [deldialogOpen, setdelDialogOpen] = useState(false);
    const [itemNumbers, setitemNumbers] = useState(0)
    const filterButton = false;
    const categoryList = true;

    // useEffect(() => {

    //     if (isLoading1 === 0) {
    //         console.log('running categories load');
    //         dispatch({ type: "ALL_CATEGORIES", payload: response1 });
    //     }
    //     if (isLoading2 === 0) {
    //         console.log('running storage load');
    //         dispatch({ type: "ALL_STORAGE", payload: response2 });
    //     }
    //     if (isLoading4 === 0) {
    //         console.log('running tags load');
    //         dispatch({ type: "ALL_TAGS", payload: response4 });
    //     }

    //     return function cleanup() {
    //         console.log('unmounting groups');
    //     }
    // }, [
    //     response1,
    //     response2,
    //     response4,
    //     isLoading1,
    //     isLoading2,
    //     isLoading4])

    useEffect(() => {
        if (page === "Category") {
            if (allCategories) {
                setgroupData(allCategories.sort(compare))
            }
        }
        if (page === "Storage") {
            if (allStorage) {
                setgroupData(allStorage.sort(compare))
            }
        }

        if (page === "Tags") {
            if (allItemTags) {
                setgroupData(allItemTags.sort(compare))
            }
        }
        return function cleanup() {
            console.log('unmounting groups');
        }
    }, [allCategories, allStorage, allItemTags, page])

    const IndCat = (el) => {
        setIndData(el);
        setDialogOpen(!dialogOpen);
        console.log(IndData);
    };

    const delIndCat = (el) => {
        setIndData(el);
        setdelDialogOpen(!dialogOpen);
        console.log(IndData);
    };


    const groupList = () => {

        let listArr = [];
        if (groupData) {
            console.log('group', groupData);
            groupData.forEach((el, i) => {
                const listStyle = {
                    borderLeft: `10px solid ${el.colour}`
                }
                let itemNumbers = []
                if (page === "Category") {
                    itemNumbers = allItems.filter(row => row.category === el.name)
                }

                if (page === "Storage") {
                    itemNumbers = allItems.filter(row => row.storage === el.name)
                }

                if (page === "Tags") {
                    itemNumbers = allItems.filter(row => row.tags && row.tags.includes(el.name))
                }

                listArr.push(
                    <Fragment key={el.id}>
                        <ListItem
                            style={listStyle}
                            key={el.id}
                            color="inherit"
                        >
                            <ListItemText primary={el.name}
                                secondary={`${itemNumbers.length} item(s)`} />
                            <ListItemSecondaryAction>
                                <Edit color='primary' button onClick={() => IndCat(el)} className={classes.iconPad} />
                                <Delete color='secondary' button onClick={() => delIndCat(el)} />
                            </ListItemSecondaryAction>
                        </ListItem>
                        <Divider />
                    </Fragment>
                );
            });
        }
        return listArr;
    };


    return (
        <>
            <Navbar
                filterButton={filterButton}
            />
            <Grid container justify="center">
                <Grid item xs={12} sm={12}>
                    <List className={classes.pageBottom} dense>{groupList()}</List>
                </Grid>
            </Grid>
            <BottomBar categoryList={categoryList} setgroupData={setgroupData} groupData={groupData} />
            <EditGroups
                IndData={IndData}
                dialogOpen={dialogOpen}
                setDialogOpen={setDialogOpen}
                groupData={groupData}
                setgroupData={setgroupData}
            />
            <DeleteGroups
                groupData={groupData}
                setgroupData={setgroupData}
                IndData={IndData}
                dialogOpen={deldialogOpen}
                setDialogOpen={setdelDialogOpen}
            />
        </>
    );
}