import React, { useEffect } from 'react'
import { Grid, Divider, makeStyles } from '@material-ui/core/'
import { AppState } from '../../context/app-context'


import useDataFetchWhere from '../../utils/useDataFetchWhere'
import CheckedList from '../checklist/checklistComponents/checkedList'
import Navbar from '../../components/navbar/navbar'
import UnCheckedList from '../checklist/checklistComponents/unCheckedList'
import CheckListComplete from '../checklist/checklistComponents/checkListComplete'

const useStyles = makeStyles((theme) => ({
    pad: {
        margin: theme.spacing(1, 0, 1, 0),
    }
}));

export default function ListPage(props) {

    const classes = useStyles();

    const [stateContext, dispatch] = AppState()
    const { uncheckedList, checkedList } = stateContext;

    const collection = "lists"
    const doc = props.location.listId
    const subCollection = "items"

    console.log('check', checkedList);

    const [response, error, isLoading] = useDataFetchWhere(collection, doc, subCollection)

    useEffect(() => {
        if (isLoading === 0) {
            const unchecked = response.filter(item => item.checked === false)
            dispatch({ type: 'UNCHECKED_LIST', payload: unchecked })
            const checked = response.filter(item => item.checked === true)
            dispatch({ type: 'CHECKED_LIST', payload: checked })
            console.log('unchecked list', unchecked);
            console.log('checked list', checked);
        }
        return function cleanup() {
            console.log('unmounting shoppingPage');
        }
    },
        [isLoading]
    );

    return (
        <>
            <Navbar />
            <Grid container spacing={3} justify="center">
                <Grid item xs={12} sm={12}>
                    {uncheckedList.length < 1 ?
                        <CheckListComplete /> :
                        <UnCheckedList doc={doc} />
                    }
                </Grid>
            </Grid>
            {checkedList.length > 0 ?
                <Divider className={classes.pad} /> : null
            }
            <Grid container spacing={3} justify="center">
                <Grid item xs={12} sm={12}>
                    <CheckedList doc={doc} />
                </Grid>
            </Grid>
        </>

    )
}
