import React, { Fragment } from 'react'
import { AppState } from '../../../context/app-context'
import {
    ListItem, ListItemText, Divider, List,
    Checkbox, ListItemSecondaryAction, makeStyles, ListItemIcon, IconButton
} from '@material-ui/core/'
import CloseIcon from '@material-ui/icons/Close';

import { uniqueCategories, updateChecked, deleteCheckHandler } from '../../../utils/actions';

const useStyles = makeStyles((theme) => ({
    catColor: {
        backgroundColor: "#e4e4e4",
    },
    noPad: { padding: theme.spacing(0, 0, 0, 0) }
}));

export default function SList(props) {

    const classes = useStyles();
    const [stateContext, dispatch] = AppState()

    const { uncheckedList, checkedList } = stateContext;

    const listId = props.doc

    const handleToggle = (value) => () => {

        let newUncheckedList = [...uncheckedList]
        let newCheckedList = [...checkedList]
        let item

        for (var i = 0; i < newUncheckedList.length; i++) {
            if (newUncheckedList[i].docId === value.docId) {
                newUncheckedList.splice(i, 1);
                item = { ...value, checked: true }
            }
        }
        newCheckedList.push(item)

        updateChecked(listId, value.docId, 'checked', true)
            .then(() => {
                console.log("updated checked");
                dispatch({ type: 'UNCHECKED_LIST', payload: newUncheckedList })
                dispatch({ type: 'CHECKED_LIST', payload: newCheckedList })
            })
            .catch((err) => {
                console.log(err)
                dispatch({ type: "SNACKBAR", payload: { open: true, message: 'Contact Admin', severity: 'danger' } })
            })

    }

    const handleDel = (value) => () => {
        let newUncheckedList = [...uncheckedList]

        for (var i = 0; i < newUncheckedList.length; i++) {
            if (newUncheckedList[i].docId === value.docId) {
                newUncheckedList.splice(i, 1);
            }
        }
        deleteCheckHandler(listId, value.docId)
            .then(() => {
                dispatch({ type: 'UNCHECKED_LIST', payload: newUncheckedList })
                console.log("deleted check item");
            })
            .catch((err) => {
                console.log(err)
                dispatch({ type: "SNACKBAR", payload: { open: true, message: 'Contact Admin', severity: 'danger' } })
            })
    }


    let finalData = []

    if (uncheckedList) {
        const uniqueCats = uniqueCategories(uncheckedList).sort()

        uniqueCats.forEach((cat) => {
            let sc = uncheckedList.filter((row) => row.category === cat);
            if (sc.length > 0) {
                finalData.push({
                    category: cat,
                    item: sc,
                });
            }
        });
    }

    const catAndItems = (finalData) => {
        let listArr = []
        finalData.forEach(data => {
            const Uncategorised = 'Uncategorised'
            listArr.push(
                <Fragment key={data.index}>
                    <List dense disablePadding>
                        <ListItem className={classes.catColor} >
                            <ListItemText primary={`${data.category === "" ? `${Uncategorised}` : `${data.category}`}`} />
                        </ListItem>
                    </List>
                </Fragment>
            )
            data.item.forEach(el => {
                const labelId = `checkbox-list-label-${el.id}`;
                listArr.push(
                    < Fragment key={el.id}>
                        <ListItem key={el.id} color='inherit'>
                            <ListItemIcon>
                                <Checkbox
                                    edge="start"
                                    checked={uncheckedList.checked}
                                    tabIndex={-1}
                                    disableRipple
                                    onChange={handleToggle(el)}
                                    inputProps={{ 'aria-labelledby': labelId }}
                                />
                            </ListItemIcon>
                            <ListItemText primary={el.name} />
                            <ListItemSecondaryAction>
                                <IconButton edge="end" aria-label="delete" onClick={handleDel(el)}>
                                    <CloseIcon fontSize="small" />
                                </IconButton>
                            </ListItemSecondaryAction>
                        </ListItem>
                        <Divider />
                    </Fragment>
                )
            })
        })
        return listArr
    }

    return (
        <List>
            {catAndItems(finalData)}
        </List>
    )


}
