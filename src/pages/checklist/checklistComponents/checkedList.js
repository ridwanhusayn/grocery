import React, { Fragment, useContext } from 'react'
import {
    Grid, ListItem, ListItemAvatar, Avatar, IconButton,
    ListItemText, Divider, List, ListItemSecondaryAction, Checkbox, makeStyles, ListItemIcon
} from '@material-ui/core/'
import CloseIcon from '@material-ui/icons/Close';
import { AppState } from '../../../context/app-context'
import { uniqueCategories, updateChecked, deleteCheckHandler } from '../../../utils/actions';

const useStyles = makeStyles((theme) => ({
    catColor: {
        backgroundColor: "#e4e4e4",
    }
}));

export default function CheckedList(props) {

    const classes = useStyles();
    const [stateContext, dispatch] = AppState()
    const { uncheckedList, checkedList } = stateContext;
    let checkedData = []
    const listId = props.doc

    const handleToggle = (value) => () => {

        let newUncheckedList = [...uncheckedList]
        let newCheckedList = [...checkedList]
        let item

        for (var i = 0; i < newCheckedList.length; i++) {
            if (newCheckedList[i].docId === value.docId) {
                newCheckedList.splice(i, 1);
                item = { ...value, checked: false }
            }
        }
        newUncheckedList.push(item)
        updateChecked(listId, value.docId, 'checked', false)
            .then(() => {
                console.log("updated unchecked");
                dispatch({ type: 'UNCHECKED_LIST', payload: newUncheckedList })
                dispatch({ type: 'CHECKED_LIST', payload: newCheckedList })
            })
            .catch((err) => {
                console.log(err)
                dispatch({ type: "SNACKBAR", payload: { open: true, message: 'Contact Admin', severity: 'danger' } })
            })

    }

    const handleDel = (value) => () => {
        let newCheckedList = [...checkedList]

        for (var i = 0; i < newCheckedList.length; i++) {
            if (newCheckedList[i].docId === value.docId) {
                newCheckedList.splice(i, 1);
            }
        }
        deleteCheckHandler(listId, value.docId)
            .then(() => {
                dispatch({ type: 'CHECKED_LIST', payload: newCheckedList })
                console.log("deleted check item");
            })
            .catch((err) => {
                console.log(err)
                dispatch({ type: "SNACKBAR", payload: { open: true, message: 'Contact Admin', severity: 'danger' } })
            })
    }

    if (checkedList) {

        const uniqueCats = uniqueCategories(checkedList).sort()

        uniqueCats.forEach((cat) => {
            let sc = checkedList.filter((row) => row.category === cat)
            if (sc.length > 0) {
                checkedData.push({
                    category: cat,
                    item: sc,
                });
            }
        });
    }

    const catAndItems = (checkedData) => {
        let listArr = []
        if (checkedData) {
            checkedData.forEach(data => {
                const Uncategorised = 'Uncategorised'
                listArr.push(
                    <Fragment key={data.index}>
                        <List dense disablePadding>
                            <ListItem className={classes.catColor} >
                                <ListItemText primary={`${data.category === "" ? `${Uncategorised}` : `${data.category}`}`} />
                            </ListItem>
                        </List>
                    </Fragment>
                )
                data.item.forEach(el => {
                    const labelId = `checkbox-list-label-${el.id}`;
                    listArr.push(
                        < Fragment key={el.id}>
                            <ListItem key={el.id} color='inherit'>
                                <ListItemIcon>
                                    <Checkbox
                                        edge="start"
                                        checked={el.checked}
                                        tabIndex={-1}
                                        disableRipple
                                        onChange={handleToggle(el)}
                                        inputProps={{ 'aria-labelledby': labelId }}
                                    />
                                </ListItemIcon>
                                <ListItemText primary={el.name} style={{ textDecoration: "line-through", fontStyle: "italic" }} />
                                <ListItemSecondaryAction>
                                    <IconButton edge="end" aria-label="delete" onClick={handleDel(el)}>
                                        <CloseIcon fontSize="small" />
                                    </IconButton>
                                </ListItemSecondaryAction>
                            </ListItem>
                            <Divider />
                        </Fragment>
                    )
                })
            })
            return listArr
        }
    }

    return (

        <List>
            {catAndItems(checkedData)}
        </List>

    )
}
