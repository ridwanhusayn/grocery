import React from 'react'
import { Grid, Typography, makeStyles } from "@material-ui/core";
import { AppState } from '../../../context/app-context'

const useStyles = makeStyles((theme) => ({
    pad: {
        padding: "10px",
    }
}));

export default function CheckListComplete() {
    const classes = useStyles();
    const [stateContext, dispatch] = AppState()
    const { uncheckedList, checkedList } = stateContext;

    return (
        <div>
            <Grid
                container
                spacing={0}
                direction="column"
                alignItems="center"
                justify="center"
            >
                <Grid item xs={10}>
                    <i> <Typography className={classes.pad} align="center" variant="h6">
                        {uncheckedList.length < 1 && checkedList < 1 ? 'Check List Empty'
                            : 'Check List Complete'}
                    </Typography> </i>
                    <i> <Typography align="center" variant="h6">
                        {uncheckedList.length < 1 && checkedList < 1 ? 'Add items from the Pantry'
                            : null}
                    </Typography> </i>
                </Grid>
            </Grid>

        </div>
    )
}

