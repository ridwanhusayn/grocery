import React, { Fragment, useEffect, useState } from "react";
import { AppState } from '../../context/app-context'
import useDataFetches from "../../utils/useDataFetches";

import {
	uniqueCategories, search, convertedDate, fiterFunctionCreator,
	filterArray, availableItems, deleteSingleHandler, compare
} from '../../utils/actions';

import Loading from '../../components/loading'
import EditItem from "../../components/modals/editItem";
import BottomBar from "../../components/bottomBar/bottomBar";
import Navbar from "../../components/navbar/navbar";
import Search from "../../components/search/search";
import NoItemsFound from "../../components/noItemsFound";
import ChangeLevel from "../../components/modals/changeLevel";

import {
	Grid, ListItem, ListItemText, Divider, List,
	ListItemSecondaryAction, Checkbox, makeStyles, Chip, Typography,
} from "@material-ui/core";
import Accordion from '@material-ui/core/Accordion';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import AccordionActions from '@material-ui/core/AccordionActions';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Button from '@material-ui/core/Button';
import EditIcon from "@material-ui/icons/Edit";

const useStyles = makeStyles((theme) => ({
	catColor: {
		backgroundColor: "#e4e4e4",
	},
	pageBottom: {
		paddingBottom: theme.spacing(7),
		paddingTop: '0px'
	},
	chipSpace: {
		marginRight: '4px',
		marginBottom: '4px',
	},
	chipFull: {
		backgroundColor: '#edf7ed'
	},
	chipLow: {
		backgroundColor: '#fff4e5'
	},
	chipTag: {
		backgroundColor: '#ff5971',
		color: 'white'
	},
	root: {
		width: '100%',
		padding: '0'
	},
	heading: {
		fontSize: theme.typography.pxToRem(15),
	},
	secondaryHeading: {
		fontSize: theme.typography.pxToRem(15),
		color: theme.palette.text.secondary,
	},
	icon: {
		verticalAlign: 'bottom',
		height: 20,
		width: 20,
	},
	column: {
		flexBasis: '50%',
	},
	helper: {
		borderLeft: `2px solid ${theme.palette.divider}`,
		padding: theme.spacing(1, 2),
	},
	link: {
		color: theme.palette.primary.main,
		textDecoration: 'none',
		'&:hover': {
			textDecoration: 'underline',
		},
	},
	accRoot: {
		"&$expanded": {
			margin: '0',
			borderBottom: '1px solid #e0e0e0'
		}
	},
	accRound: {
		borderRadius: '0',
		"&:first-child": {
			borderTopLeftRadius: '0',
			borderTopRightRadius: '0'
		}
	},
	accSumRoot: {
		padding: '0 16px 0 0'
	},
	accSumCont: {
		margin: '0',
		"&$expanded": {
			margin: '0',
		},
	},
	expanded: {},
	accSumexp: {
		margin: '0'
	},
	listItm: {
		paddingLeft: theme.spacing(1),
	}
}));

export default function Home() {

	console.log('home page loaded');
	const classes = useStyles();
	const [response, error, isLoading] = useDataFetches("items");
	const [response3, error3, isLoading3] = useDataFetches("lists");
	const [response1, error1, isLoading1] = useDataFetches("category");
	const [response2, error2, isLoading2] = useDataFetches("storage");
	const [response4, error4, isLoading4] = useDataFetches("tags");
	const [stateContext, dispatch] = AppState()

	const { allItems, pantryOn, filterOn, searchWord,
		displayItemstoShow, filterCheckList, checked } = stateContext;

	const [IndData, setIndData] = useState([]);
	const [chipOpen, setchipOpen] = useState(false)
	const [chipData, setchipData] = useState([])
	const [dialogOpen, setDialogOpen] = useState(false);
	const filterButton = true;
	const items = true;

	useEffect(() => {
		if (isLoading === 0) {
			console.log('running allitems load');
			dispatch({ type: "INIT", payload: response });
		}
		if (isLoading3 === 0) {
			console.log('running lists load');
			dispatch({ type: "ALL_LIST", payload: response3 });
		}
		if (isLoading1 === 0) {
			console.log('running categories load');
			dispatch({ type: "ALL_CATEGORIES", payload: response1.sort(compare) });
		}
		if (isLoading2 === 0) {
			console.log('running storage load');
			dispatch({ type: "ALL_STORAGE", payload: response2.sort(compare) });
		}
		if (isLoading4 === 0) {
			console.log('running tags load');
			dispatch({ type: "ALL_TAGS", payload: response4.sort(compare) });
		}
		return function cleanup() {
			console.log("unmounting home data load");
		}

	}, [
		response, response1, response2, response4, response3,
		isLoading, isLoading1, isLoading3, isLoading2, isLoading4
	]
	);

	useEffect(() => {
		if (pantryOn) {
			let pantryList = availableItems(allItems)
			dispatch({ type: "DISPLAYITEMS", payload: pantryList });
		}
		if (!pantryOn) {
			dispatch({ type: "DISPLAYITEMS", payload: allItems });
		}

		if (filterOn > 0) {
			let fit = []
			if (pantryOn) {
				fit = availableItems(allItems)
			} else {
				fit = allItems
			}
			let filters = fiterFunctionCreator(filterCheckList)
			let newlist = filterArray(fit, filters)
			dispatch({ type: "DISPLAYITEMS", payload: newlist });
		}

		if (searchWord) {
			let s = search(searchWord, displayItemstoShow)
			dispatch({ type: "DISPLAYITEMS", payload: s });
		}

		return function cleanup() {
			console.log('unmounting displayData');
		}
	}, [allItems, pantryOn, filterOn, searchWord])


	const handleToggle = (value) => () => {
		const currentIndex = checked.indexOf(value);
		const newChecked = [...checked];
		if (currentIndex === -1) {
			newChecked.push(value);
		} else {
			newChecked.splice(currentIndex, 1);
		}
		dispatch({ type: "CHECKED", payload: newChecked });
		console.log("checked", checked);
	};

	const IndItem = (el) => {
		setIndData(el);
		setDialogOpen(!dialogOpen);
		console.log('indivdual Item', IndData);
	};

	const handleSingleDel = id => {
		deleteSingleHandler(id).then(() => {
			console.log("deleted");
			dispatch({
				type: "SNACKBAR", payload:
					{ open: true, message: 'Successfully Deleted Item', severity: 'success' }
			});
		})
			.catch((err) => {
				console.log(err)
				dispatch({ type: "SNACKBAR", payload: { open: true, message: 'Contact Admin', severity: 'danger' } })
			});
	}

	const handleLevel = el => {
		setchipOpen(!chipOpen)
		setchipData(el)
	}

	const handleChipClose = () => setchipOpen(false);

	const cardViews = () => {

		const finalData = []
		let listArr = [];

		if (displayItemstoShow) {
			console.log('displai', displayItemstoShow);
			const uniqueCats = uniqueCategories(displayItemstoShow).sort()

			uniqueCats.forEach((cat) => {
				let sc = displayItemstoShow.filter((row) => row.category === cat);
				if (sc.length > 0) {
					finalData.push({
						category: cat,
						item: sc,
					});
				}
			});
		}

		if (finalData) {
			finalData.forEach((data, i) => {
				const Uncategorised = 'Uncategorised'
				listArr.push(
					<Fragment>
						<ListItem className={classes.catColor}>
							<ListItemText primary={`${data.category === "" ? `${Uncategorised}` : `${data.category}`} (${data.item.length})`} />
						</ListItem>
						<Divider />
					</Fragment>
				);
				data.item.forEach((el) => {
					const labelId = `checkbox-list-label-${el.id}`;
					listArr.push(
						<Fragment key={el.id}>
							<Accordion
								key={el.id}
								classes={{
									root: classes.accRoot,
									expanded: classes.expanded,
									rounded: classes.accRound
								}}>
								<AccordionSummary
									expandIcon={<ExpandMoreIcon />}
									aria-controls="panel1c-content"
									id="panel1c-header"
									classes={{
										root: classes.accSumRoot,
										content: classes.accSumCont,
										expanded: classes.expanded
									}}
								>
									<List
										onClick={(event) => event.stopPropagation()}
										onFocus={(event) => event.stopPropagation()}
										className={classes.root}
									>
										<ListItem
											color="inherit"
											className={classes.listItm}
										>
											<Checkbox
												checked={checked.indexOf(el) !== -1}
												tabIndex={-1}
												disableRipple
												inputProps={{ "aria-labelledby": labelId }}
												onChange={handleToggle(el)}
											/>
											<ListItemText primary={el.name} secondary={el.storage ? `${el.storage}` : "None"} />
											<ListItemSecondaryAction>
												<Chip label={el.level} onClick={() => handleLevel(el)}
													className={el.level === 'Full' ? classes.chipFull : el.level === 'Low' ? classes.chipLow : null}
												></Chip>
											</ListItemSecondaryAction>
										</ListItem>
									</List>
								</AccordionSummary>
								<AccordionDetails className={classes.details}>
									<div className={classes.column}>
										<Typography className={classes.secondaryHeading}>{el.quantity ? `${el.quantity} Units` : `N/A`}</Typography>
										<Typography className={classes.secondaryHeading}>{el.expiryDate ? `Exp: ${convertedDate(el.expiryDate)}` : "Exp:"}</Typography>
									</div>
									<div className={classes.column}>
										<div className={classes.helper}>
											{el.tags ? el.tags.map(tag => {
												return <Chip
													classes={{
														root: classes.chipSpace
													}}
													label={tag} size="small" className={classes.chipTag} />
											}) : null}

										</div>
									</div>
								</AccordionDetails>
								<Divider />
								<AccordionActions>
									<Button size="small" onClick={() => handleSingleDel(el.id)} >Delete</Button>
									<Button size="small" color="primary" onClick={() => IndItem(el)}>Edit</Button>
								</AccordionActions>
							</Accordion>
						</Fragment >

					);
				});
			});
		}
		return listArr;
	}

	return (
		<>
			<Navbar filterButton={filterButton} />
			<Search />
			{isLoading === 1 ?
				<><Loading /></>
				:
				<Grid container justify="center">
					<Grid item xs={12} sm={12}>
						{cardViews()}
					</Grid>
				</Grid>
			}
			{(!displayItemstoShow.length > 0 && filterOn) || (!displayItemstoShow.length > 0 && searchWord) ?
				<NoItemsFound /> : null
			}
			<BottomBar items={items} />
			<EditItem
				IndData={IndData}
				dialogOpen={dialogOpen}
				setDialogOpen={setDialogOpen}
			/>
			<ChangeLevel
				handleChipClose={handleChipClose}
				open={chipOpen}
				chipData={chipData} />
		</>
	);
}