import React, { Fragment, useEffect, useState } from "react";
import { AppState } from '../../context/app-context'
import { compare } from '../../utils/actions';
import EditGroups from "../../components/modals/editGroups";
import DeleteGroups from "../../components/modals/deleteGroups";
import BottomBar from "../../components/bottomBar/bottomBar";
import Navbar from "../../components/navbar/navbar";
import AddList from "../../components/modals/addList";
import EditList from "../../components/modals/editList";
import DeleteList from "../../components/modals/deleteList";

import {
    Grid, ListItem, ListItemText, Divider, List, IconButton, ListItemSecondaryAction,
    Checkbox, makeStyles,
} from "@material-ui/core";
import { Delete, Edit } from "@material-ui/icons/";

const useStyles = makeStyles((theme) => ({
    catColor: {
        backgroundColor: "#e4e4e4",
    },
    pageBottom: {
        paddingBottom: theme.spacing(7),
        paddingTop: "0px"
    },
    iconPad: {
        paddingRight: "5px"
    }
}));


export default function ListGroups() {

    const classes = useStyles();
    const [
        stateContext, dispatch,
    ] = AppState()
    const { allList } = stateContext;


    const [IndData, setIndData] = useState([]);
    const [dialogOpen, setDialogOpen] = useState(false);
    const [deldialogOpen, setdelDialogOpen] = useState(false);
    const [itemNumbers, setitemNumbers] = useState(0)
    const filterButton = false;
    const categoryList = true;



    const IndCat = (el) => {
        setIndData(el);
        setDialogOpen(!dialogOpen);
        console.log(IndData);
    };

    const delIndCat = (el) => {
        setIndData(el);
        setdelDialogOpen(!dialogOpen);
        console.log(IndData);
    };


    const groupList = () => {

        let listArr = [];
        if (allList) {
            console.log('listing ', allList)
            allList.forEach((el, i) => {
                const listStyle = {
                    borderLeft: `10px solid ${el.colour}`
                }

                listArr.push(
                    <Fragment key={el.id}>
                        <ListItem
                            style={listStyle}
                            key={el.id}
                            color="inherit"
                        >
                            <ListItemText primary={el.name}
                            // secondary={`${itemNumbers.length} item(s)`} 
                            />
                            <ListItemSecondaryAction>
                                <Edit color='primary' button onClick={() => IndCat(el)} className={classes.iconPad} />
                                <Delete color='secondary' button onClick={() => delIndCat(el)} />
                            </ListItemSecondaryAction>
                        </ListItem>
                        <Divider />
                    </Fragment>
                );
            });
        }
        return listArr;
    };


    return (
        <>
            <Navbar navbarTitle={'Lists'}
                filterButton={filterButton}
            />
            <Grid container justify="center">
                <Grid item xs={12} sm={12}>
                    <List className={classes.pageBottom} dense>{groupList()}</List>
                </Grid>
            </Grid>
            <BottomBar allList={true} />
            {/* <EditGroups
                IndData={IndData}
                dialogOpen={dialogOpen}
                setDialogOpen={setDialogOpen}
                groupData={groupData}
                setgroupData={setgroupData}
            />
            <DeleteGroups
                groupData={groupData}
                setgroupData={setgroupData}
                IndData={IndData}
                dialogOpen={deldialogOpen}
                setDialogOpen={setdelDialogOpen}
            /> */}
            <EditList
                IndData={IndData}
                dialogOpen={dialogOpen}
                setDialogOpen={setDialogOpen}
            />
            <DeleteList
                IndData={IndData}
                dialogOpen={deldialogOpen}
                setDialogOpen={setdelDialogOpen}
            />
        </>
    );
}