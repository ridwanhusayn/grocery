import React from "react";

import CircularProgress from '@material-ui/core/CircularProgress';
import LinearProgress from '@material-ui/core/LinearProgress';
import { Grid } from "@material-ui/core";


export default function Loading() {
    return (
        <div>
            <Grid
                container
                spacing={0}
                direction="column"
                alignItems="center"
                justify="center"
                style={{ minHeight: '50vh' }}
            >
                <Grid item xs={10}>
                    <CircularProgress
                        color="secondary"
                        variant="indeterminate"
                        size={40}
                        thickness={4}
                        value={40} />
                </Grid>
            </Grid>
            <Grid
                container
                spacing={0}
                alignItems="center"
                justify="center"
            >
                <Grid item xs={10}>
                    <LinearProgress />
                </Grid>

            </Grid>
        </div>
    )
}
