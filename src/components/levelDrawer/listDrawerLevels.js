import React, { Fragment, useState, useContext } from "react";
import { AppState } from '../../context/app-context'

import {
	List,
	ListItem,
	ListItemText,
	ListItemIcon,
	IconButton,
	makeStyles,
	Divider,
	Drawer,
	Checkbox,
} from "@material-ui/core";
import KeyboardBackspaceIcon from "@material-ui/icons/KeyboardBackspace";
import { Link } from "react-router-dom";



const useStyles = makeStyles((theme) => ({
	nested: {
		paddingLeft: theme.spacing(4),
	},
	drawerList: {
		width: 300,
		maxWidth: 360,
		backgroundColor: theme.palette.background.paper,
	},
}));

export default function ListDrawerLevels(props) {
	const classes = useStyles();
	const [
		stateContext, dispatch
	] = AppState()
	const { filterCheckList } = stateContext;

	const levchecked = props.levChecked;
	const setlevChecked = props.setlevChecked;


	const handleToggle = (value) => () => {
		const currentIndex = filterCheckList.levels.indexOf(value);
		const newChecked = [...filterCheckList.levels];

		if (currentIndex === -1) {
			newChecked.push(value);
		} else {
			newChecked.splice(currentIndex, 1);
		}

		setlevChecked(newChecked);
		dispatch({ type: "FILTERED_CHECKLIST", payload: { ...filterCheckList, levels: newChecked } });

	};

	const back = () => {
		props.setLevelOpen(false);
		console.log("levcheck", levchecked)
		props.setfilterLevel(levchecked);
	};

	const levels = () => {
		let listArr = [];
		if (props.levFilter) {
			props.levFilter.forEach((data) => {
				let sc = props.filterableItems ? props.filterableItems.filter((row) => row.level === data).length : 0
				listArr.push(
					<Fragment key={data}>
						<List component="div" disablePadding>
							<ListItem button color="inherit">
								<ListItemIcon>
									<Checkbox
										edge="start"
										checked={levchecked.indexOf(data) !== -1}
										tabIndex={-1}
										disableRipple
										onChange={handleToggle(data)}
									/>
								</ListItemIcon>
								<ListItemText primary={`${data}`} />
							</ListItem>
						</List>
						<Divider />
					</Fragment>
				);
			});
			return listArr;
		}
	};

	return (
		<>
			<Drawer anchor="right" open={props.levelOpen}>
				<div className={classes.drawerList}>
					<List component="nav" aria-label="secondary mailbox folders">
						<ListItem>
							<ListItemIcon>
								<IconButton>
									<KeyboardBackspaceIcon onClick={back} />
								</IconButton>
							</ListItemIcon>
							<ListItemText primary="Levels" />
						</ListItem>
						<Divider />
						{levels()}
						<Divider />
					</List>
				</div>
			</Drawer>
		</>
	);
}
