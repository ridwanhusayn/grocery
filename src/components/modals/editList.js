import React, { useState, useEffect } from 'react'
import { AppState } from '../../context/app-context'
import {
    Grid, FormControl, TextField,
    Button, Typography, DialogTitle, DialogContent, DialogActions, Dialog
} from '@material-ui/core'
import { editGroupHandler } from '../../utils/actions';

import SnackBar from '../snackbar'

export default function EditList(props) {

    const [
        stateContext, dispatch
    ] = AppState()
    const { allList } = stateContext;


    const [updatedList, setupdatedList] = useState([])

    let dialogOpen = props.dialogOpen
    let setDialogOpen = props.setDialogOpen
    let listData = props.IndData

    useEffect(() => {
        if (listData) {
            setupdatedList(listData)
        }
        return function cleanup() {
            console.log('unmouting editList');
        }
    }, [listData]);


    const handleChange = name => ({ target: { value } }) => {
        setupdatedList({
            ...updatedList,
            [name]: value
        });
    }
    const handleCancel = () => {
        setDialogOpen(!dialogOpen)
    }


    const AddupdatedList = e => {
        e.preventDefault()

        editGroupHandler(updatedList, 'lists')
            .then(() => {
                console.log("editted list", updatedList);
                dispatch({
                    type: "SNACKBAR", payload: {
                        open: true, message: `Successfully Edited ${updatedList.name}`, severity: 'success'
                    }
                });
                const editState = allList.filter((el) => el.id !== updatedList.id);
                editState.push(updatedList);
                dispatch({ type: "ALL_LIST", payload: editState });

            })
            .catch((err) => {
                console.log(err)
                dispatch({ type: "SNACKBAR", payload: { open: true, message: 'Contact Admin', severity: 'danger' } })
            });
        setDialogOpen(!dialogOpen)
    }

    return (
        <>
            <SnackBar>
            </SnackBar>
            <Dialog
                disableBackdropClick
                disableEscapeKeyDown
                maxWidth="xs"
                aria-labelledby="confirmation-dialog-title"
                open={dialogOpen}

            >
                <DialogTitle id="confirmation-dialog-title">Edit List</DialogTitle>
                <div>
                    <DialogContent dividers>
                        <Grid container spacing={3}
                            direction="row"
                            justify="center"
                            alignItems="center"
                        >
                            <Grid item xs={12}>
                                <FormControl fullWidth variant="outlined">
                                    <TextField
                                        fullWidth id="name"
                                        label={`List Name`}
                                        type="text"
                                        name="name"
                                        value={(updatedList ? updatedList.name : " ")}
                                        onChange={handleChange("name")}
                                        variant="outlined" />
                                </FormControl>
                            </Grid>

                        </Grid>
                    </DialogContent>
                    <DialogActions>
                        <Button autoFocus onClick={handleCancel} color="primary">
                            Cancel
                        </Button>
                        <Button
                            onClick={AddupdatedList}
                            color="primary"
                            disabled={updatedList.length !== 0 ? false : true}
                        >
                            Save
                        </Button>
                    </DialogActions>
                </div>
            </Dialog>
        </>
    )
}