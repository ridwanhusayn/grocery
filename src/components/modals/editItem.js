import React, { useEffect, useState, useContext } from 'react'
import { useForm, Form } from '../../utils/useForm';
import { editHandler, getCurrentDate, forDateDns } from '../../utils/actions';
import { DebounceInput } from 'react-debounce-input';
import {
    Grid, FormControl, TextField, MenuItem,
    InputLabel, Select, Button, Input, Chip,
    DialogTitle, DialogContent, DialogActions, FormHelperText
} from '@material-ui/core'
import { KeyboardDatePicker } from "@material-ui/pickers";
import { makeStyles, useTheme } from '@material-ui/core/styles';
import Dialog from '@material-ui/core/Dialog';

import { AppState } from '../../context/app-context'
import { AuthContext } from '../../utils/auth'

import SnackBar from '../snackbar'

const useStyles = makeStyles((theme) => ({
    chips: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    chip: {
        margin: 2,
        backgroundColor: '#ff5971',
        color: 'white'
    }
}));

const initialFValues = {
    name: '',
    category: '',
    storage: '',
    quantity: '',
    level: '',
    expiryDate: null,
    tags: [],
    lastModifiedBy: '',
    lastModifiedDate: ''

}

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
    PaperProps: {
        style: {
            maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
            width: 250,
        },
    },
};

export default function EditItem(props) {

    console.log('editItem modal loaded');
    const [
        stateContext, dispatch
    ] = AppState()
    const { allCategories, allStorage, allItemTags } = stateContext;
    const { currentUser } = useContext(AuthContext)

    const classes = useStyles();
    const theme = useTheme();

    const [itemData, setCurrentItemData] = useState({})
    // const [selectedDate, handleDateChange] = useState(null);
    const [tags, settags] = useState([]);

    let dialogOpen = props.dialogOpen
    let setDialogOpen = props.setDialogOpen

    useEffect(() => {
        if (props.IndData) {
            setValues(props.IndData)
            console.log('setting ind data');

            if (props.IndData.expiryDate) {
                console.log('setting ind ex');
                let cDate = forDateDns(props.IndData.expiryDate)
                handleDateChange(cDate)
            } else {
                handleDateChange(null)
            }

        }
        return function cleanup() {
            console.log('unmouting editItem');
        }
    }, [props.IndData]);

    const validate = (fieldValues = values) => {
        let temp = { ...errors }
        if ('name' in fieldValues)
            temp.name = fieldValues.name ? "" : "This field is required."
        // if ('category' in fieldValues)
        //     temp.category = fieldValues.category ? "" : "This field is required."
        if ('level' in fieldValues)
            temp.level = fieldValues.level ? "" : "This field is required."
        setErrors({
            ...temp
        })

        if (fieldValues == values)
            return Object.values(temp).every(x => x == "")
    }

    const {
        values,
        setValues,
        errors,
        setErrors,
        handleInputChange,
        resetForm,
        changeMade,
        setchangeMade,
        selectedDate,
        handleDateChange,
        exDate
    } = useForm(initialFValues, true, validate);

    const handleSubmit = e => {
        e.preventDefault()
        if (validate()) {
            setCurrentItemData(
                itemData.id = values.id,
                itemData.name = values.name,
                itemData.category = values.category,
                itemData.storage = values.storage,
                itemData.quantity = values.quantity,
                itemData.level = values.level,
                itemData.tags = values.tags,
                itemData.lastModifiedBy = currentUser.displayName,
                itemData.lastModifiedDate = getCurrentDate(),
                selectedDate && values.level === "Out" ? itemData.expiryDate = "" : itemData.expiryDate = selectedDate
            )

            console.log(itemData);
            editHandler(itemData)
                .then(() => {
                    console.log("Edited Item");
                    dispatch({ type: "SNACKBAR", payload: { open: true, message: 'Successfully Edited Item', severity: 'success' } });
                })
                .catch((err) => {
                    console.log(err)
                    dispatch({ type: "SNACKBAR", payload: { open: true, message: `Something went wrong - ${err}`, severity: 'danger' } });
                })
            setDialogOpen(!dialogOpen)
            setCurrentItemData({})
        }
    }

    const handleCancel = () => {
        setErrors({})
        setValues(props.IndData)
        setDialogOpen(!dialogOpen)
        setCurrentItemData({})
        if (props.IndData.expiryDate) {
            console.log('setting ind ex');
            let cDate = forDateDns(props.IndData.expiryDate)
            handleDateChange(cDate)
        } else {
            handleDateChange(null)
        }
    }

    return (
        <>
            <SnackBar>
            </SnackBar>

            <Dialog
                disableBackdropClick
                disableEscapeKeyDown
                maxWidth="xs"
                aria-labelledby="confirmation-dialog-title"
                open={dialogOpen}

            >
                <DialogTitle id="confirmation-dialog-title">Edit Item</DialogTitle>
                <div>
                    <Form onSubmit={handleSubmit}>
                        <DialogContent dividers>
                            <Grid container spacing={3} justify="center">
                                <Grid item xs={12}>
                                    <FormControl fullWidth variant="outlined">
                                        <DebounceInput
                                            element={TextField}
                                            minLength={1}
                                            debounceTimeout={300}
                                            fullWidth
                                            label="Item Name"
                                            type="text"
                                            variant="outlined"
                                            name="name"
                                            value={values.name}
                                            onChange={handleInputChange}
                                            error={errors.name ? true : false}
                                            helperText={errors.name ? errors.name : null}
                                        />
                                    </FormControl>
                                </Grid>
                                <Grid item xs={12}>
                                    <FormControl fullWidth variant="outlined" error={errors.category ? true : false}>
                                        <InputLabel>Category</InputLabel>
                                        <Select
                                            fullWidth
                                            label="Category"
                                            MenuProps={MenuProps}
                                            name="category"
                                            value={values.category}
                                            onChange={handleInputChange}
                                        >
                                            <MenuItem value="">None</MenuItem>
                                            {allCategories.map(cat => {
                                                return <MenuItem key={cat.id} value={`${cat.name}`}>{cat.name}</MenuItem>
                                            })}
                                        </Select>
                                        <FormHelperText>{errors.category ? errors.category : false}</FormHelperText>
                                    </FormControl>
                                </Grid>
                                <Grid item xs={12}>
                                    <FormControl fullWidth variant="outlined" error={errors.storage ? true : false}>
                                        <InputLabel>Storage</InputLabel>
                                        <Select
                                            fullWidth
                                            label="Storage"
                                            MenuProps={MenuProps}
                                            name="storage"
                                            value={values.storage}
                                            onChange={handleInputChange}
                                        >
                                            <MenuItem value="">None</MenuItem>
                                            {allStorage.map(store => {
                                                return <MenuItem key={store.id} value={`${store.name}`}>{store.name}</MenuItem>
                                            })}
                                        </Select>
                                        <FormHelperText>{errors.storage ? errors.storage : false}</FormHelperText>
                                    </FormControl>
                                </Grid>
                                <Grid item xs={12}>
                                    <FormControl fullWidth variant="outlined">
                                        <TextField
                                            fullWidth
                                            label="Quantity"
                                            type="number"
                                            variant="outlined"
                                            name="quantity"
                                            InputProps={{ inputProps: { min: 0 } }}
                                            value={values.quantity}
                                            onChange={handleInputChange}
                                            error={errors.quantity ? true : false}
                                            helperText={errors.quantity ? errors.quantity : null}
                                        />
                                    </FormControl>
                                </Grid>
                                <Grid item xs={12}>
                                    <FormControl fullWidth variant="outlined" error={errors.level ? true : false}>
                                        <InputLabel>Level</InputLabel>
                                        <Select
                                            fullWidth
                                            label="Level"
                                            MenuProps={MenuProps}
                                            name="level"
                                            value={values.level}
                                            onChange={handleInputChange}
                                        >
                                            <MenuItem value={"Full"}>Full</MenuItem>
                                            <MenuItem value={"Low"}>Low</MenuItem>
                                            <MenuItem value={"Out"}>Out</MenuItem>
                                        </Select>
                                        <FormHelperText>{errors.level ? errors.level : false}</FormHelperText>
                                    </FormControl>
                                </Grid>
                                <Grid item xs={12}>
                                    <FormControl fullWidth variant="outlined">
                                        <KeyboardDatePicker
                                            autoOk
                                            name='expiryDate'
                                            value={selectedDate}
                                            onChange={date => exDate(date)}
                                            format="dd-MM-yyyy"
                                            variant="inline"
                                            inputVariant="outlined"
                                            label="Expiry Date"
                                            InputAdornmentProps={{ position: "end" }}
                                        />
                                    </FormControl>
                                </Grid>
                                <Grid item xs={12}>
                                    <FormControl fullWidth variant="outlined">
                                        <InputLabel id="demo-mutiple-chip-label">Tags</InputLabel>
                                        <Select
                                            multiple
                                            name="tags"
                                            value={values.tags}
                                            onChange={handleInputChange}
                                            MenuProps={MenuProps}
                                            input={<Input id="select-multiple-chip" />}
                                            renderValue={(selected) => (
                                                <div
                                                    className={classes.chips}
                                                >
                                                    {selected.map((value) => (
                                                        <Chip key={value} label={value}
                                                            className={classes.chip}
                                                        />
                                                    ))}
                                                </div>
                                            )}
                                            MenuProps={MenuProps}
                                        >
                                            {allItemTags.map(tag => {
                                                return <MenuItem key={tag.id} value={`${tag.name}`}>{tag.name}</MenuItem>
                                            })}
                                        </Select>
                                    </FormControl>
                                </Grid>
                            </Grid>
                        </DialogContent>
                        <DialogActions>
                            <Button autoFocus color="primary" onClick={handleCancel}>Cancel</Button>
                            <Button type="submit" color="primary" disabled={changeMade ? false : true}>Save</Button>
                        </DialogActions>
                    </Form>
                </div>
            </Dialog>
        </>
    )
}