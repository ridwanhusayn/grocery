import React, { useState } from 'react'
import { AppState } from '../../context/app-context'
import {
    Grid, FormControl, TextField,
    Button, Typography, DialogTitle, DialogContent, DialogActions, Dialog
} from '@material-ui/core'
import { deletegroupHandler, getSpecificItems, removeFieldValueItems } from '../../utils/actions';
import SnackBar from '../snackbar'

export default function DeleteList(props) {

    const [
        stateContext, dispatch
    ] = AppState()
    const { allList, page } = stateContext;

    let dialogOpen = props.dialogOpen
    let setDialogOpen = props.setDialogOpen
    const IndData = props.IndData

    // let itemNumbers = []
    // if (page) {
    //     console.log(page);
    //     if (page === "Category") {
    //         itemNumbers = allItems.filter(row => row.category === IndData.name)
    //     }

    //     if (page === "Storage") {
    //         itemNumbers = allItems.filter(row => row.storage === IndData.name)
    //     }

    //     if (page === "Tag") {
    //         itemNumbers = allItems.filter(row => row.tags && row.tags.includes(IndData.name))
    //     }
    // }


    const deleteList = () => {

        deletegroupHandler(IndData.id, 'lists')
            .then(() => {
                console.log('deleted group', IndData.id)
                const deleteState = allList.filter((el) => el.id !== IndData.id)
                dispatch({ type: "ALL_LIST", payload: deleteState });
                dispatch({ type: "SNACKBAR", payload: { open: true, message: `Successfully Deleted ${IndData.name}`, severity: 'success' } });
                setDialogOpen(!dialogOpen)
            })
            .catch((err) => {
                console.log(err)
                dispatch({ type: "SNACKBAR", payload: { open: true, message: 'Contact Admin', severity: 'danger' } })
            });
    }

    const handleCancel = () => {
        setDialogOpen(!dialogOpen)
    }

    return (
        <>
            <SnackBar>
            </SnackBar>
            <Dialog
                disableBackdropClick
                disableEscapeKeyDown
                maxWidth="xs"
                aria-labelledby="confirmation-dialog-title"
                open={dialogOpen}

            >
                <DialogTitle id="confirmation-dialog-title" align={'center'} >Delete {props.IndData.name} </DialogTitle>
                <div>
                    <DialogContent dividers>
                        <Grid container spacing={3}
                            direction="row"
                            justify="center"
                            alignItems="center"
                        >
                            <Typography variant={'body1'} align={'center'}>Are you sure want to delete this {props.IndData.name}?</Typography>
                            <Typography variant={'caption'} align={'center'}>Complete checklist will be removed.</Typography>
                        </Grid>
                    </DialogContent>
                    <DialogActions>
                        <Button autoFocus onClick={handleCancel} color="primary">
                            Cancel
                        </Button>
                        <Button
                            onClick={() => deleteList()}
                            color="primary"
                        >
                            Delete
                        </Button>
                    </DialogActions>
                </div>
            </Dialog>
        </>
    )
}