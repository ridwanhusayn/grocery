import React, { useState } from 'react'
import { AppState } from '../../context/app-context'
import {
    Grid, FormControl, TextField,
    Button, Typography, DialogTitle, DialogContent, DialogActions, Dialog, colors
} from '@material-ui/core'
import { ColorPicker } from 'material-ui-color';
import { addGroupHandler } from '../../utils/actions';

import SnackBar from '../snackbar'

export default function AddGroups(props) {

    const [
        stateContext, dispatch
    ] = AppState()
    const { page } = stateContext;


    const [newGrpData, setnewGrpData] = useState([])
    let dialogOpen = props.dialogOpen
    let setDialogOpen = props.setDialogOpen
    const [color, setColor] = useState('transparent');
    const groupData = props.groupData
    const setgroupData = props.setgroupData

    const handleChange1 = (value) => {
        setColor(value);
    };

    const handleChange = name => ({ target: { value } }) => {
        setnewGrpData({
            ...newGrpData,
            [name]: value
        });
    }
    const handleCancel = () => {
        setDialogOpen(!dialogOpen)
    }


    const AddnewGrpData = e => {
        e.preventDefault()
        if (colors) {
            const hex1 = `#${color.hex}`
            setnewGrpData(newGrpData.colour = hex1)
        }

        console.log(newGrpData)
        addGroupHandler(newGrpData, page)
            .then((res) => {
                console.log("new group created", res.id);
                setnewGrpData(newGrpData.id = res.id)
                setgroupData([...groupData, newGrpData])
                setnewGrpData([])
                dispatch({ type: "SNACKBAR", payload: { open: true, message: `Successfully Added ${page}`, severity: 'success' } });
                console.log(groupData);

            })
            .catch((err) => {
                console.log(err)
                dispatch({ type: "SNACKBAR", payload: { open: true, message: 'Contact Admin', severity: 'danger' } })
            })

        props.setOpen(false)
        setDialogOpen(!dialogOpen)
    }

    return (
        <>
            <SnackBar>
            </SnackBar>
            <Dialog
                disableBackdropClick
                disableEscapeKeyDown
                maxWidth="xs"
                aria-labelledby="confirmation-dialog-title"
                open={dialogOpen}

            >
                <DialogTitle id="confirmation-dialog-title">Add {page}</DialogTitle>
                <div>
                    <DialogContent dividers>
                        <Grid container spacing={3}
                            direction="row"
                            justify="center"
                            alignItems="center"
                        >
                            <Grid item xs={12}>
                                <FormControl fullWidth variant="outlined">
                                    <TextField
                                        fullWidth id="name"
                                        label={`${page} Name`}
                                        type="text"
                                        name="name"
                                        value={(newGrpData ? newGrpData.name : " ")}
                                        onChange={handleChange("name")}
                                        variant="outlined" />
                                </FormControl>
                            </Grid>
                            {page === 'Tags' ? null :
                                <>
                                    <Grid item xs={2}>
                                        <ColorPicker
                                            hideTextfield
                                            onChange={handleChange1}
                                            value={color}
                                        />
                                    </Grid>
                                    <Grid item xs={8}>
                                        <Typography> Choose Colour</Typography>
                                    </Grid>
                                </>
                            }
                        </Grid>
                    </DialogContent>
                    <DialogActions>
                        <Button autoFocus onClick={handleCancel} color="primary">
                            Cancel
                        </Button>
                        <Button
                            onClick={AddnewGrpData}
                            color="primary"
                            disabled={newGrpData.length !== 0 ? false : true}
                        >
                            Save
                        </Button>
                    </DialogActions>
                </div>
            </Dialog>
        </>
    )
}