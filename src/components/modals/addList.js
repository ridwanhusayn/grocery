import React, { useState } from 'react'
import { AppState } from '../../context/app-context'
import {
    Grid, FormControl, TextField,
    Button, Typography, DialogTitle, DialogContent, DialogActions, Dialog
} from '@material-ui/core'
import { addListHandler } from '../../utils/actions';

import SnackBar from '../snackbar'

export default function AddList(props) {

    const [
        stateContext, dispatch
    ] = AppState()
    const { allList } = stateContext;


    const [newList, setnewList] = useState([])

    let dialogOpen = props.addListDialogue
    let setDialogOpen = props.setaddListDialogue

    const handleChange = name => ({ target: { value } }) => {
        setnewList({
            ...newList,
            [name]: value
        });
    }
    const handleCancel = () => {
        setDialogOpen(!dialogOpen)
    }


    const AddnewList = e => {
        e.preventDefault()

        addListHandler(newList, 'lists')
            .then((res) => {
                console.log("new list created", res.id);
                dispatch({
                    type: "SNACKBAR", payload: {
                        open: true, message: `Successfully Added ${newList.name}`, severity: 'success'
                    }
                });
                let revisedList = [...allList]
                revisedList.push(newList)
                dispatch({ type: "ALL_LIST", payload: revisedList });
                props.setOpen(false)
                setDialogOpen(!dialogOpen)

            })
            .catch((err) => {
                console.log(err)
                dispatch({ type: "SNACKBAR", payload: { open: true, message: 'Contact Admin', severity: 'danger' } })
            })
    }

    return (
        <>
            <SnackBar>
            </SnackBar>
            <Dialog
                disableBackdropClick
                disableEscapeKeyDown
                maxWidth="xs"
                aria-labelledby="confirmation-dialog-title"
                open={dialogOpen}

            >
                <DialogTitle id="confirmation-dialog-title">Add List</DialogTitle>
                <div>
                    <DialogContent dividers>
                        <Grid container spacing={3}
                            direction="row"
                            justify="center"
                            alignItems="center"
                        >
                            <Grid item xs={12}>
                                <FormControl fullWidth variant="outlined">
                                    <TextField
                                        fullWidth id="name"
                                        label={`List Name`}
                                        type="text"
                                        name="name"
                                        value={(newList ? newList.name : " ")}
                                        onChange={handleChange("name")}
                                        variant="outlined" />
                                </FormControl>
                            </Grid>

                        </Grid>
                    </DialogContent>
                    <DialogActions>
                        <Button autoFocus onClick={handleCancel} color="primary">
                            Cancel
                        </Button>
                        <Button
                            onClick={AddnewList}
                            color="primary"
                            disabled={newList.length !== 0 ? false : true}
                        >
                            Save
                        </Button>
                    </DialogActions>
                </div>
            </Dialog>
        </>
    )
}