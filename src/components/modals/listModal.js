import React, { useState, useEffect } from 'react'
import {
    FormControl, MenuItem,
    Select, Button, DialogTitle, DialogContent,
    DialogActions, Dialog, DialogContentText, makeStyles
} from '@material-ui/core'

// import useDataFetches from '../../data/useDataFetches'
import { AppState } from '../../context/app-context'
import { addItemSC } from '../../utils/actions'

const useStyles = makeStyles((theme) => ({
    formControl: {
        marginTop: theme.spacing(2),
        minWidth: 120,
    },
    formControlLabel: {
        marginTop: theme.spacing(1),
    },
}));

export default function ListModal(props) {

    const classes = useStyles();
    const [
        stateContext, dispatch
    ] = AppState()
    const { allList, checked } = stateContext;

    // const [listResponse, listError, listIsLoading] = useDataFetches('lists')
    // copy listresponse to listData.. Also listdata 
    const [listData, setlistData] = useState([])
    const [selectedList, setSelectedList] = useState('')
    // to display once a list has been selected
    const [listDisplay, setlistDisplay] = useState('')

    let listdialogOpen = props.listdialogOpen
    let setlistdialogOpen = props.setlistdialogOpen

    const handleChanges = e => {
        setlistDisplay(e.target.value);
        setSelectedList(allList[e.target.value].id)
    }


    const handleCancel = e => {
        props.setOpen(false)
        setlistdialogOpen(!listdialogOpen)

    }

    const addItemsToList = () => {
        props.setOpen(false)
        setlistdialogOpen(!listdialogOpen)
        console.log('list sel', selectedList);

        let newChecked = []
        checked.forEach(el => {
            let checking = {
                name: el.name,
                checked: false,
                category: el.category
            }
            newChecked.push(checking)
        });

        addItemSC(newChecked, selectedList).then(() => {
            console.log("added items to list");
            dispatch({ type: "SNACKBAR", payload: { open: true, message: 'Successfully Added Item(s)', severity: 'success' } });
        })
            .catch((err) => {
                console.log(err)
                dispatch({ type: "SNACKBAR", payload: { open: true, message: 'Contact Admin', severity: 'danger' } })
            });

        dispatch({ type: "CHECKED", payload: [] });
    }

    return (

        <Dialog
            fullWidth
            maxWidth="sm"
            open={listdialogOpen}
            aria-labelledby="max-width-dialog-title"
        >
            <DialogTitle id="max-width-dialog-title">List</DialogTitle>
            <DialogContent dividers>
                <DialogContentText>
                    Choose a list to add selected items
                </DialogContentText>
                <FormControl fullWidth className={classes.formControl} variant="outlined">
                    <Select
                        labelId="list"
                        id="list"
                        value={listDisplay}
                        onChange={handleChanges}
                        displayEmpty
                    >
                        <MenuItem value="" disabled>
                            <em>Select list</em>
                        </MenuItem>
                        {allList.length > 0 ? allList.map((list, idx) => {
                            return <MenuItem key={list.id} data-id={list.id} value={idx}>{list.name}</MenuItem>
                        }) : null
                        }
                    </Select>
                </FormControl>
            </DialogContent>
            <DialogActions>
                <Button autoFocus onClick={handleCancel} color="primary">
                    Cancel
                </Button>
                <Button
                    onClick={addItemsToList}
                    color="primary"
                    disabled={selectedList.length > 0 ? false : true}
                >
                    Add To List
                </Button>
            </DialogActions>
        </Dialog>
    )
}




