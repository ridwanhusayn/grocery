import React, { useState, useEffect } from 'react'
import { AppState } from '../../context/app-context'
import {
    Grid, FormControl, InputLabel, Select,
    Button, MenuItem, DialogTitle, DialogContent, DialogActions, Dialog, colors
} from '@material-ui/core'
import { updateLevel } from '../../utils/actions';

import SnackBar from '../snackbar'

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
    PaperProps: {
        style: {
            maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
            width: 250,
        },
    },
};

export default function ChangeLevel(props) {
    console.log('changeLevel modal Loaded');
    const [
        stateContext, dispatch
    ] = AppState()

    const [itemData, setitemData] = useState([])
    const cData = props.chipData

    console.log('chip', itemData);

    useEffect(() => {
        if (cData) {
            setitemData(cData)
        }
        return function cleanup() {
            console.log('unmouting changeLevel');
        }
    }, [cData]);


    const handleInputChange = name => ({ target: { value } }) => {
        setitemData({
            ...itemData,
            [name]: value
        });

        updateLevel(itemData.id, "level", value)
            .then((res) => {
                console.log("updated Level");
                dispatch({ type: "SNACKBAR", payload: { open: true, message: `Successfully Updated ${itemData.name} to ${value}`, severity: 'success' } });
                props.handleChipClose()

            })
            .catch((err) => {
                console.log(err)
                dispatch({ type: "SNACKBAR", payload: { open: true, message: 'Contact Admin', severity: 'danger' } })
            })

    }


    // const AddnewGrpData = e => {
    //     e.preventDefault()
    //     if (colors) {
    //         const hex1 = `#${color.hex}`
    //         setnewGrpData(newGrpData.colour = hex1)
    //     }

    //     console.log(newGrpData)
    //     addGroupHandler(newGrpData, page)
    //         .then((res) => {
    //             console.log("new group created", res.id);
    //             setnewGrpData(newGrpData.id = res.id)
    //             setgroupData([...groupData, newGrpData])
    //             setnewGrpData([])
    //             dispatch({ type: "SNACKBAR", payload: { open: true, message: `Successfully Added ${page}`, severity: 'success' } });
    //             console.log(groupData);

    //         })
    //         .catch((err) => console.log(err))

    //     props.setOpen(false)
    //     setDialogOpen(!dialogOpen)
    // }

    return (
        <>
            <SnackBar>
            </SnackBar>
            <Dialog
                maxWidth="xs"
                aria-labelledby="confirmation-dialog-title"
                open={props.open}
                onClose={props.handleChipClose}

            >
                <DialogTitle id="confirmation-dialog-title">{props.chipData.name}</DialogTitle>
                <div>
                    <DialogContent dividers>
                        <Grid container spacing={3}
                            direction="row"
                            justify="center"
                            alignItems="center"
                        >
                            <Grid item xs={12}>
                                <FormControl fullWidth variant="outlined">
                                    <InputLabel>Level</InputLabel>
                                    <Select
                                        fullWidth
                                        label="Level"
                                        MenuProps={MenuProps}
                                        name="level"
                                        value={itemData ? itemData.level : " "}
                                        onChange={handleInputChange("level")}
                                    >
                                        <MenuItem value={"Full"}>Full</MenuItem>
                                        <MenuItem value={"Low"}>Low</MenuItem>
                                        <MenuItem value={"Out"}>Out</MenuItem>
                                    </Select>
                                </FormControl>
                            </Grid>
                        </Grid>
                    </DialogContent>
                </div>
            </Dialog>
        </>
    )
}
