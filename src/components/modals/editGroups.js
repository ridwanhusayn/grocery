import React, { useEffect, useState } from 'react'
import { AppState } from '../../context/app-context'
import {
    Grid, FormControl, TextField,
    Button, Typography, DialogTitle, DialogContent, DialogActions, Dialog
} from '@material-ui/core'
import { ColorPicker } from 'material-ui-color';
import { editGroupHandler } from '../../utils/actions';

export default function EditGroups(props) {

    const [
        stateContext, dispatch
    ] = AppState()
    const { page } = stateContext;

    const [grpData, setCurrentgrpData] = useState([])
    let dialogOpen = props.dialogOpen
    let setDialogOpen = props.setDialogOpen
    const [color, setColor] = useState('red');
    const groupData = props.groupData
    const setgroupData = props.setgroupData


    useEffect(() => {
        if (props.IndData) {
            setCurrentgrpData(props.IndData)
            setColor(props.IndData.colour)
        }
        return function cleanup() {
            console.log('unmouting editGroup');
        }
    }, [props.IndData]);

    const handleChange1 = (value) => {
        setColor(value);
    };

    const handleChange = name => ({ target: { value } }) => {
        setCurrentgrpData({
            ...grpData,
            [name]: value
        });
    }
    const handleCancel = () => {
        setDialogOpen(!dialogOpen)
    }


    const EditgrpData = e => {
        e.preventDefault()
        if (color) {
            const hex1 = `#${color.hex}`
            setCurrentgrpData(grpData.colour = hex1)
        }


        console.log(grpData);
        editGroupHandler(grpData, page)
            .then(() => {
                console.log("editted group", groupData);
                const editState = groupData.filter((el) => el.id !== grpData.id);
                console.log('before push', editState);
                editState.push(grpData);
                console.log('after push', editState);
                setgroupData([...editState])
            })
            .catch((err) => {
                console.log(err)
                dispatch({ type: "SNACKBAR", payload: { open: true, message: 'Contact Admin', severity: 'danger' } })
            });
        setCurrentgrpData([])
        setDialogOpen(!dialogOpen)
    }

    return (

        <Dialog
            disableBackdropClick
            disableEscapeKeyDown
            maxWidth="xs"
            aria-labelledby="confirmation-dialog-title"
            open={dialogOpen}
        >
            <DialogTitle id="confirmation-dialog-title">Edit {page}</DialogTitle>
            <div>
                <DialogContent dividers>
                    <Grid container spacing={3}
                        direction="row"
                        justify="center"
                        alignItems="center"
                    >
                        <Grid item xs={12}>
                            <FormControl fullWidth variant="outlined">
                                <TextField
                                    fullWidth id="name"
                                    label={`${page} Name`}
                                    type="text"
                                    name="name"
                                    value={(grpData ? grpData.name : " ")}
                                    onChange={handleChange("name")}
                                    variant="outlined" />
                            </FormControl>
                        </Grid>
                        {page === 'Tags' ? null :
                            <>
                                <Grid item xs={2}>
                                    <ColorPicker
                                        hideTextfield
                                        onChange={handleChange1}
                                        value={color}
                                    />
                                </Grid>
                                <Grid item xs={8}>
                                    <Typography> Choose Colour</Typography>
                                </Grid>
                            </>
                        }
                    </Grid>
                </DialogContent>
                <DialogActions>
                    <Button autoFocus onClick={handleCancel} color="primary">
                        Cancel
                    </Button>
                    <Button
                        onClick={EditgrpData}
                        color="primary"
                        disabled={grpData ? false : true}
                    >
                        Save
                    </Button>
                </DialogActions>
            </div>
        </Dialog>
    )
}