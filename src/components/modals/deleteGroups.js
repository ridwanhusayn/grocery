import React, { useState } from 'react'
import { AppState } from '../../context/app-context'
import {
    Grid, FormControl, TextField,
    Button, Typography, DialogTitle, DialogContent, DialogActions, Dialog
} from '@material-ui/core'
import { deletegroupHandler, getSpecificItems, removeFieldValueItems } from '../../utils/actions';
import SnackBar from '../snackbar'

export default function DeleteCatergories(props) {

    const [
        stateContext, dispatch
    ] = AppState()
    const { allItems, page } = stateContext;

    let dialogOpen = props.dialogOpen
    let setDialogOpen = props.setDialogOpen
    const groupData = props.groupData
    const setgroupData = props.setgroupData
    const IndData = props.IndData

    let itemNumbers = []
    if (page) {
        console.log(page);
        if (page === "Category") {
            itemNumbers = allItems.filter(row => row.category === IndData.name)
        }

        if (page === "Storage") {
            itemNumbers = allItems.filter(row => row.storage === IndData.name)
        }

        if (page === "Tag") {
            itemNumbers = allItems.filter(row => row.tags && row.tags.includes(IndData.name))
        }
    }


    const delGroup = () => {
        // deleted All categories first
        const field = page.toLowerCase()
        const value = IndData.name
        let res = null
        console.log(field, value);

        getSpecificItems(field, value)
            .then((data) => {

                res = data.docs.map(doc => ({ ...doc.data(), id: doc.id }))
                if (data.docs.length > 0) {
                    removeFieldValueItems(res, field, value)
                        .then(data => {
                            console.log('updated items aswell');
                            deletegroupHandler(IndData.id, page)
                                .then(() => {
                                    console.log('deleted group', IndData.id)
                                    setgroupData(groupData.filter((el) => el.id !== IndData.id))
                                    dispatch({ type: "SNACKBAR", payload: { open: true, message: `Successfully Deleted ${page}`, severity: 'success' } });
                                })
                                .catch((err) => {
                                    console.log(err)
                                    dispatch({ type: "SNACKBAR", payload: { open: true, message: 'Contact Admin', severity: 'danger' } })
                                });
                        })
                        .catch((error) => {
                            console.log("Error deleting items: ", error);
                        });
                }
                if (!data.docs.length > 0) {
                    deletegroupHandler(IndData.id, page)
                        .then(() => {
                            console.log('deleted group', IndData.id)
                            setgroupData(groupData.filter((el) => el.id !== IndData.id))
                            dispatch({ type: "SNACKBAR", payload: { open: true, message: `Successfully Deleted ${page}`, severity: 'success' } });
                        })
                        .catch((err) => {
                            console.log(err)
                            dispatch({ type: "SNACKBAR", payload: { open: true, message: 'Contact Admin', severity: 'danger' } })
                        });
                }
            })
            .catch((error) => {
                console.log("Error getting documents: ", error);
            });
        setDialogOpen(!dialogOpen)
    }

    const handleCancel = () => {
        setDialogOpen(!dialogOpen)
    }



    return (
        <>
            <SnackBar>
            </SnackBar>
            <Dialog
                disableBackdropClick
                disableEscapeKeyDown
                maxWidth="xs"
                aria-labelledby="confirmation-dialog-title"
                open={dialogOpen}

            >
                <DialogTitle id="confirmation-dialog-title" align={'center'} >Delete {props.IndData.name} {page} </DialogTitle>
                <div>
                    <DialogContent dividers>
                        <Grid container spacing={3}
                            direction="row"
                            justify="center"
                            alignItems="center"
                        >
                            <Typography variant={'body1'} align={'center'}>Are you sure want to delete this {page}?</Typography>
                            <Typography variant={'caption'} align={'center'}>It has {itemNumbers.length} items where {page} will be removed.</Typography>
                        </Grid>
                    </DialogContent>
                    <DialogActions>
                        <Button autoFocus onClick={handleCancel} color="primary">
                            Cancel
                        </Button>
                        <Button
                            onClick={() => delGroup()}
                            color="primary"
                        >
                            Delete
                        </Button>
                    </DialogActions>
                </div>
            </Dialog>
        </>
    )
}