import React, { useContext, Fragment } from "react";
import { AppState } from '../../context/app-context'

import {
	List,
	ListItem,
	ListItemText,
	ListItemIcon,
	makeStyles,
	Divider,
	Drawer,
	Button,
	Checkbox,
	IconButton,
} from "@material-ui/core";
import KeyboardBackspaceIcon from "@material-ui/icons/KeyboardBackspace";


const useStyles = makeStyles((theme) => ({
	nested: {
		paddingLeft: theme.spacing(4),
	},
	drawerList: {
		width: 300,
		maxWidth: 360,
		backgroundColor: theme.palette.background.paper,
	},
}));

export default function ListDrawerCatergories(props) {
	const classes = useStyles();
	const [
		stateContext, dispatch

	] = AppState()
	const { filterCheckList } = stateContext;

	const catchecked = props.catChecked;
	const setcatChecked = props.setcatChecked;

	const handleToggle = (value) => () => {
		const currentIndex = filterCheckList.categories.indexOf(value);
		const newChecked = [...filterCheckList.categories];
		console.log(currentIndex);
		if (currentIndex === -1) {
			newChecked.push(value);
		} else {
			newChecked.splice(currentIndex, 1);
		}

		setcatChecked(newChecked);
		dispatch({ type: "FILTERED_CHECKLIST", payload: { ...filterCheckList, categories: newChecked } });
	};

	const back = () => {
		props.setCategoryOpen(false);
		props.setfilterCategories(catchecked);

	};

	//Get list of categories
	const categories = () => {
		let listArr = [];
		if (props.catsFilter) {
			props.catsFilter.forEach((data) => {
				const Uncategorised = 'Uncategorised'
				// let sc = props.filterableItems ? props.filterableItems.filter((row) => row.category === data).length : 0
				listArr.push(
					<Fragment key={data}>
						<List component="div" disablePadding>
							<ListItem button color="inherit">
								<ListItemIcon>
									<Checkbox
										edge="start"
										checked={filterCheckList.categories.indexOf(data) !== -1}
										tabIndex={-1}
										disableRipple
										onChange={handleToggle(data)}
									/>
								</ListItemIcon>
								<ListItemText primary={`${data === "" ? `${Uncategorised}` : `${data}`}`} />
							</ListItem>
						</List>
						<Divider />
					</Fragment>
				);
			});
			return listArr;
		}
	};

	return (
		<>
			<Drawer anchor="right" open={props.categoryOpen}>
				<div className={classes.drawerList}>
					<List component="nav" aria-label="secondary mailbox folders">
						<ListItem>
							<ListItemIcon >
								<IconButton>
									<KeyboardBackspaceIcon onClick={back} />
								</IconButton>
							</ListItemIcon>
							<ListItemText primary="Categories" />
						</ListItem>
						<Divider />
						{categories()}
						<Divider />
					</List>
				</div>
			</Drawer>
		</>
	);
}
