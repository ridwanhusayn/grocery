import React, { Fragment, useState, useContext } from "react";
import { AppState } from '../../context/app-context'

import {
    List,
    ListItem,
    ListItemText,
    ListItemIcon,
    makeStyles,
    Divider,
    Drawer,
    Checkbox,
    IconButton,
} from "@material-ui/core";
import KeyboardBackspaceIcon from "@material-ui/icons/KeyboardBackspace";

const useStyles = makeStyles((theme) => ({
    nested: {
        paddingLeft: theme.spacing(4),
    },
    drawerList: {
        width: 300,
        maxWidth: 360,
        backgroundColor: theme.palette.background.paper,
    },
}));

export default function ListDrawerTags(props) {
    const classes = useStyles();
    const [
        stateContext, dispatch
    ] = AppState()

    const { filterCheckList } = stateContext;

    const tagchecked = props.tagsChecked;
    const tagsChecked = props.settagsChecked;


    const handleToggle = (value) => () => {
        const currentIndex = filterCheckList.tags.indexOf(value);
        const newChecked = [...filterCheckList.tags];

        if (currentIndex === -1) {
            newChecked.push(value);
        } else {
            newChecked.splice(currentIndex, 1);
        }

        tagsChecked(newChecked);
        dispatch({ type: "FILTERED_CHECKLIST", payload: { ...filterCheckList, tags: newChecked } });
    };

    const back = () => {
        props.settagsOpen(false);
        props.setfilterTags(tagchecked);
    };

    const tagslist = () => {
        let listArr = [];
        if (props.tagFilter) {
            props.tagFilter.forEach((data) => {
                let sc = props.filterableItems.filter(row => row.tags ? row.tags.some(tag => tag === data) : null).length
                listArr.push(
                    <Fragment key={data}>
                        <List component="div" disablePadding>
                            <ListItem button color="inherit">
                                <ListItemIcon>
                                    <Checkbox
                                        edge="start"
                                        checked={tagchecked.indexOf(data) !== -1}
                                        tabIndex={-1}
                                        disableRipple
                                        onChange={handleToggle(data)}
                                    />
                                </ListItemIcon>
                                <ListItemText primary={`${data}`} />
                            </ListItem>
                        </List>
                        <Divider />
                    </Fragment>
                );
            });
            return listArr;
        }
    };

    return (
        <>
            <Drawer anchor="right" open={props.tagsOpen}>
                <div className={classes.drawerList}>
                    <List component="nav" aria-label="secondary mailbox folders">
                        <ListItem>
                            <ListItemIcon>
                                <IconButton>
                                    <KeyboardBackspaceIcon onClick={back} />
                                </IconButton>
                            </ListItemIcon>
                            <ListItemText primary="Tags" />
                        </ListItem>
                        <Divider />
                        {tagslist()}
                        <Divider />
                    </List>
                </div>
            </Drawer>
        </>
    );
}
