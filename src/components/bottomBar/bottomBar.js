import React, { useState } from "react";
import { AppState } from '../../context/app-context'
import { deleteHandler } from '../../utils/actions';

import { AppBar, Toolbar, Grid, makeStyles } from "@material-ui/core";

import Button from "@material-ui/core/Button";
import DeleteIcon from "@material-ui/icons/Delete";
import FileCopyIcon from "@material-ui/icons/FileCopyOutlined";
import AddIcon from "@material-ui/icons/Add";
import SelectAllIcon from '@material-ui/icons/SelectAll';

import AddItem from "../modals/addItemNew";
import ListModal from "../modals/listModal";
import AddGroups from '../modals/addGroups'
import AddList from '../modals/addList'
import SnackBar from '../snackbar'


const useStyles = makeStyles((theme) => ({
	appBar: {
		top: "auto",
		bottom: 0,
		background: "white",
	},
	label: {
		flexDirection: "column",
		display: "block",
	},
	bottombarText: {
		fontSize: "9px",
	},
}));

const BottomBar = (props) => {
	console.log('bottombar loaded');
	const classes = useStyles();
	const [
		stateContext, dispatch
	] = AppState()
	const { displayItemstoShow, page, checked } = stateContext;


	const [open, setOpen] = useState(false);
	const [dialogOpen, setDialogOpen] = useState(false);
	const [addListDialogue, setaddListDialogue] = useState(false)
	const [listdialogOpen, setlistdialogOpen] = useState(false);
	const [catdialogOpen, setcatdialogOpen] = useState(false)
	const [selectAll, setselectAll] = useState(false)
	const setgroupData = props.setgroupData
	const groupData = props.groupData

	const handleAdd = () => {
		setOpen(false);
		setDialogOpen(!dialogOpen);
	};

	const handleAddcat = () => {
		setOpen(false);
		setcatdialogOpen(!dialogOpen);
	};

	const handleAddList = () => {
		setOpen(false);
		setaddListDialogue(!addListDialogue);
	};

	const handleDelete = () => {
		let ids = [];

		checked.forEach((element) => {
			ids.push(element.id);
		});
		console.log('deleted ids', ids);

		deleteHandler(ids)
			.then(() => {
				console.log("deleted");
				dispatch({ type: "SNACKBAR", payload: { open: true, message: 'Successfully Deleted Item(s)', severity: 'success' } });
			})
			.catch((err) => {
				console.log(err)
				dispatch({ type: "SNACKBAR", payload: { open: true, message: 'Contact Admin', severity: 'danger' } })
			});

		setOpen(false);
		dispatch({ type: "CHECKED", payload: [] });
	};

	const addToList = () => {
		setOpen(false);
		setlistdialogOpen(!listdialogOpen);
	};

	const handleSelectAll = () => {
		let newCheckedList = []
		dispatch({ type: "CHECKED", payload: [] });
		displayItemstoShow.forEach((data) => {
			newCheckedList.push(data)
		})
		console.log('new', newCheckedList);
		dispatch({ type: "CHECKED", payload: newCheckedList });
		setselectAll(true)
	}

	const handleUnselectAll = () => {
		dispatch({ type: "CHECKED", payload: [] });
		setselectAll(false)
	}

	return (
		<>
			<SnackBar>
			</SnackBar>
			<div className={classes.grow}>
				<AppBar position="fixed" className={classes.appBar}>
					<Toolbar>
						<Grid
							container
							direction="row"
							justify="center"
							alignItems="center"
							spacing={1}
						>
							{props.items ? (
								<Grid item>
									<Button
										color="primary"
										className={classes.label}
										onClick={handleAdd}
									>
										<AddIcon />
										<div className={classes.bottombarText}>Add Item</div>
									</Button>
								</Grid>) : null}
							{props.items && checked.length !== 0 ? (
								<Grid item>
									<Button
										color="primary"
										className={classes.label}
										onClick={addToList}
									>
										<FileCopyIcon />
										<div className={classes.bottombarText}>Add To List</div>
									</Button>
								</Grid>
							) : null}
							{props.items && checked.length !== 0 ? (
								<Grid item>
									<Button
										color="primary"
										className={classes.label}
										onClick={handleDelete}
									>
										<DeleteIcon />
										<div className={classes.bottombarText}>Delete Item</div>
									</Button>
								</Grid>
							) : null}
							{props.categoryList ? (
								<Grid item>
									<Button
										color="primary"
										className={classes.label}
										onClick={handleAddcat}
									>
										<AddIcon />
										<div className={classes.bottombarText}>Add {page}</div>
									</Button>
								</Grid>
							) : null}
							{props.items && !selectAll ? (
								<Grid item>
									<Button
										color="primary"
										className={classes.label}
										onClick={handleSelectAll}
									>
										<SelectAllIcon />
										<div className={classes.bottombarText}>Select All</div>
									</Button>
								</Grid>
							) : null}
							{props.items && selectAll ? (
								<Grid item>
									<Button
										color="primary"
										className={classes.label}
										onClick={handleUnselectAll}
									>
										<SelectAllIcon />
										<div className={classes.bottombarText}>Unselect All</div>
									</Button>
								</Grid>
							) : null}
							{props.allList ? (
								<Grid item>
									<Button
										color="primary"
										className={classes.label}
										onClick={handleAddList}
									>
										<AddIcon />
										<div className={classes.bottombarText}>New List</div>
									</Button>
								</Grid>
							) : null}
						</Grid>
					</Toolbar>
				</AppBar>
				<AddItem
					dialogOpen={dialogOpen}
					setDialogOpen={setDialogOpen}
					setOpen={setOpen}
					open={open}
				/>
				<ListModal
					listdialogOpen={listdialogOpen}
					setlistdialogOpen={setlistdialogOpen}
					setOpen={setOpen}
					open={open}
				/>
				<AddGroups
					groupData={groupData}
					setgroupData={setgroupData}
					dialogOpen={catdialogOpen}
					setDialogOpen={setcatdialogOpen}
					setOpen={setOpen}
					open={open}
				/>
				<AddList
					addListDialogue={addListDialogue}
					setaddListDialogue={setaddListDialogue}
					setOpen={setOpen}
					open={open}
				/>
			</div>
		</>
	);
};

export default BottomBar;
