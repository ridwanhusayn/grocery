import React, { Fragment, useState, useContext } from "react";
import { AppState } from '../../context/app-context'

import {
    List,
    ListItem,
    ListItemText,
    ListItemIcon,
    IconButton,
    makeStyles,
    Divider,
    Drawer,
    Checkbox,
} from "@material-ui/core";
import KeyboardBackspaceIcon from "@material-ui/icons/KeyboardBackspace";
import { Link } from "react-router-dom";



const useStyles = makeStyles((theme) => ({
    nested: {
        paddingLeft: theme.spacing(4),
    },
    drawerList: {
        width: 300,
        maxWidth: 360,
        backgroundColor: theme.palette.background.paper,
    },
}));

export default function ListDrawerStorage(props) {
    const classes = useStyles();
    const [
        stateContext, dispatch
    ] = AppState()

    const { filterCheckList } = stateContext;

    const storchecked = props.storeChecked;
    const storeChecked = props.setstoreChecked;


    const handleToggle = (value) => () => {
        const currentIndex = filterCheckList.storages.indexOf(value);
        const newChecked = [...filterCheckList.storages];

        if (currentIndex === -1) {
            newChecked.push(value);
        } else {
            newChecked.splice(currentIndex, 1);
        }

        storeChecked(newChecked);
        dispatch({ type: "FILTERED_CHECKLIST", payload: { ...filterCheckList, storages: newChecked } });
    };

    const back = () => {
        props.setstorageOpen(false);
        props.setfilterStorage(storchecked);
    };

    const storage = () => {
        let listArr = [];
        if (props.storeFilter) {
            props.storeFilter.forEach((data) => {
                // let sc = props.filterableItems ? props.filterableItems.filter((row) => row.storage === data).length : 0
                const Unstoraged = 'Unstoraged'
                listArr.push(
                    <Fragment key={data}>
                        <List component="div" disablePadding>
                            <ListItem button color="inherit">
                                <ListItemIcon>
                                    <Checkbox
                                        edge="start"
                                        checked={storchecked.indexOf(data) !== -1}
                                        tabIndex={-1}
                                        disableRipple
                                        onChange={handleToggle(data)}
                                    />
                                </ListItemIcon>
                                <ListItemText primary={`${data === "" ? `${Unstoraged}` : `${data}`}`} />
                            </ListItem>
                        </List>
                        <Divider />
                    </Fragment>
                );
            });
            return listArr;
        }
    };

    return (
        <>
            <Drawer anchor="right" open={props.storageOpen}>
                <div className={classes.drawerList}>
                    <List component="nav" aria-label="secondary mailbox folders">
                        <ListItem>
                            <ListItemIcon>
                                <IconButton>
                                    <KeyboardBackspaceIcon onClick={back} />
                                </IconButton>
                            </ListItemIcon>
                            <ListItemText primary="Storage" />
                        </ListItem>
                        <Divider />
                        {storage()}
                        <Divider />
                    </List>
                </div>
            </Drawer>
        </>
    );
}
