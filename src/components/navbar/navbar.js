import React, { useState } from "react";
import { AppState } from '../../context/app-context'
import {
	AppBar, Toolbar, fade, makeStyles, IconButton, Typography, Badge,
	MenuItem, Menu, Drawer, List
} from "@material-ui/core";

import ListDrawerLeft from "../menuDrawer/listDrawerLeft";
import Filter from "../filter/filter";

import MenuIcon from "@material-ui/icons/Menu";
import AccountCircle from "@material-ui/icons/AccountCircle";
import MailIcon from "@material-ui/icons/Mail";
import NotificationsIcon from "@material-ui/icons/Notifications";

const useStyles = makeStyles((theme) => ({
	offset: theme.mixins.toolbar,
	root: {
		background:
			"linear-gradient(45deg, rgb(33, 150, 243) 30%, rgb(33, 203, 243) 90%)",
	},
	grow: {
		flexGrow: 1,
	},
	nested: {
		paddingLeft: theme.spacing(4),
	},
	menuButton: {
		marginRight: theme.spacing(2),
	},
	search: {
		position: "relative",
		borderRadius: theme.shape.borderRadius,
		backgroundColor: fade(theme.palette.common.white, 0.15),
		"&:hover": {
			backgroundColor: fade(theme.palette.common.white, 0.25),
		},
		marginRight: theme.spacing(2),
		marginLeft: 0,
		width: "100%",
		[theme.breakpoints.up("sm")]: {
			marginLeft: theme.spacing(3),
			width: "auto",
		},
	},
	searchIcon: {
		padding: theme.spacing(0, 2),
		height: "100%",
		position: "absolute",
		pointerEvents: "none",
		display: "flex",
		alignItems: "center",
		justifyContent: "center",
	},
	inputRoot: {
		color: "inherit",
	},
	inputInput: {
		padding: theme.spacing(1, 1, 1, 0),
		// vertical padding + font size from searchIcon
		paddingLeft: `calc(1em + ${theme.spacing(4)}px)`,
		transition: theme.transitions.create("width"),
		width: "100%",
		[theme.breakpoints.up("md")]: {
			width: "20ch",
		},
	},
	sectionDesktop: {
		display: "none",
		[theme.breakpoints.up("md")]: {
			display: "flex",
		},
	},
	sectionMobile: {
		display: "flex",
		[theme.breakpoints.up("md")]: {
			display: "none",
		},
	},
	drawerList: {
		width: 300,
		maxWidth: 360,
		backgroundColor: theme.palette.background.paper,
	},
}));

export default function Navbar(props) {
	console.log('navbar loaded');
	const classes = useStyles();
	const [
		stateContext, dispatch

	] = AppState()
	const { page } = stateContext;
	const [anchorEl, setAnchorEl] = useState(null);
	const [mobileMoreAnchorEl, setMobileMoreAnchorEl] = useState(null);
	const [state, setState] = useState({ open: false });

	const togglePamtry = (pantryon, pageName) => () => {
		dispatch({ type: "FILTERED_CHECKLIST", payload: { categories: [], storages: [], levels: [], tags: [] } });
		dispatch({ type: "PANTRYON", payload: pantryon });
		setState({ open: !state.open });
		dispatch({ type: "PAGE", payload: pageName });
	};

	const toggleDrawer = () => {
		setState({ open: !state.open });
	};

	const toggleGroup = (pageName) => () => {
		setState({ open: !state.open });
		dispatch({ type: "PAGE", payload: pageName });
	};

	const listDrawer = () => (
		<div role="presentation" onKeyDown={toggleDrawer}>
			<div className={classes.drawerList}>
				<ListDrawerLeft togglePamtry={togglePamtry} toggleGroup={toggleGroup} />
			</div>
		</div>
	);

	const isMenuOpen = Boolean(anchorEl);
	const isMobileMenuOpen = Boolean(mobileMoreAnchorEl);

	const handleProfileMenuOpen = (event) => {
		setAnchorEl(event.currentTarget);
	};

	const handleMobileMenuClose = () => {
		setMobileMoreAnchorEl(null);
	};

	const handleMenuClose = () => {
		setAnchorEl(null);
		handleMobileMenuClose();
	};

	const handleMobileMenuOpen = (event) => {
		setMobileMoreAnchorEl(event.currentTarget);
	};

	const menuId = "primary-search-account-menu";
	const renderMenu = (
		<Menu
			anchorEl={anchorEl}
			anchorOrigin={{ vertical: "top", horizontal: "right" }}
			id={menuId}
			keepMounted
			transformOrigin={{ vertical: "top", horizontal: "right" }}
			open={isMenuOpen}
			onClose={handleMenuClose}
		>
			<MenuItem onClick={handleMenuClose}>Profile</MenuItem>
		</Menu>
	);

	const mobileMenuId = "primary-search-account-menu-mobile";
	const renderMobileMenu = (
		<Menu
			anchorEl={mobileMoreAnchorEl}
			anchorOrigin={{ vertical: "top", horizontal: "right" }}
			id={mobileMenuId}
			keepMounted
			transformOrigin={{ vertical: "top", horizontal: "right" }}
			open={isMobileMenuOpen}
			onClose={handleMobileMenuClose}
		>
			<MenuItem>
				<IconButton aria-label="show 4 new mails" color="inherit">
					<Badge badgeContent={4} color="secondary">
						<MailIcon />
					</Badge>
				</IconButton>
				<p>Messages</p>
			</MenuItem>
			<MenuItem>
				<IconButton aria-label="show 11 new notifications" color="inherit">
					<Badge badgeContent={11} color="secondary">
						<NotificationsIcon />
					</Badge>
				</IconButton>
				<p>Notifications</p>
			</MenuItem>
			<MenuItem onClick={handleProfileMenuOpen}>
				<IconButton
					aria-label="account of current user"
					aria-controls="primary-search-account-menu"
					aria-haspopup="true"
					color="inherit"
				>
					<AccountCircle />
				</IconButton>
				<p>Profile</p>
			</MenuItem>
		</Menu>
	);

	return (
		<React.Fragment>
			<Drawer open={state.open} onClose={toggleDrawer}>
				{listDrawer()}
			</Drawer>

			<div className={classes.grow}>
				<AppBar position="fixed" className={classes.root}>
					<Toolbar>
						<IconButton
							edge="start"
							className={classes.menuButton}
							color="inherit"
							aria-label="open drawer"
							onClick={toggleDrawer}
						>
							<MenuIcon />
						</IconButton>
						<Typography className={classes.title} variant="h6" noWrap>
							{page}
						</Typography>
						<div className={classes.grow} />
						{props.filterButton ? <Filter /> : null}
					</Toolbar>
				</AppBar>
				<div className={classes.offset} />
				{renderMobileMenu}
				{renderMenu}
			</div>
		</React.Fragment>
	);
}
