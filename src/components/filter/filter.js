import React, { useEffect, useState } from "react";
import { AppState } from '../../context/app-context'
import { availableItems } from '../../utils/actions';

import ListDrawerCatergories from "../categoryDrawer/listDrawerCatergories";
import ListDrawerLevels from "../levelDrawer/listDrawerLevels";
import ListDrawerStorage from '../storageDrawer/listDrawerStorage'
import ListDrawerTags from '../tagDrawer/listDrawerTags'

import {
	makeStyles, IconButton, Drawer, List, ListItem, ListItemText, ListItemSecondaryAction,
	Divider, Button, Badge, Typography, BottomNavigation, BottomNavigationAction,
} from "@material-ui/core/";
import CheckIcon from "@material-ui/icons/Check";
import FilterListIcon from "@material-ui/icons/FilterList";

const useStyles = makeStyles((theme) => ({
	drawerList: {
		width: 300,
		maxWidth: 360,
		backgroundColor: theme.palette.background.paper,
	},
	filterButton: {
		padding: theme.spacing(1, 1, 1, 1),
		position: "fixed",
		bottom: 0,
		width: '300px'

	},
	filterIconColor: {
		color: "#ffffff",
	},
}));

export default function Filter(props) {
	const [
		stateContext, dispatch

	] = AppState()
	const { allItems, pantryOn, filterCheckList } = stateContext;

	const classes = useStyles();

	const [open, setOpen] = useState(false);

	const [filterableItems, setfilterableItems] = useState([])
	const [invisible, setinvisible] = useState(true)

	const [categoryOpen, setCategoryOpen] = useState(false);
	const [levelOpen, setLevelOpen] = useState(false);
	const [storageOpen, setstorageOpen] = useState(false)
	const [tagsOpen, settagsOpen] = useState(false)

	const [catChecked, setcatChecked] = useState([])
	const [levChecked, setlevChecked] = useState([])
	const [storeChecked, setstoreChecked] = useState([])
	const [tagsChecked, settagsChecked] = useState([])

	const [filterLevel, setfilterLevel] = useState([]);
	const [filterCategories, setfilterCategories] = useState([]);
	const [filterStorage, setfilterStorage] = useState([])
	const [filterTags, setfilterTags] = useState([])

	const [storeFilter, setstoreFilter] = useState([])
	const [catsFilter, setcatsFilter] = useState([]);
	const [levFilter, setlevFilter] = useState([]);
	const [tagFilter, settagFilter] = useState([])

	useEffect(() => {
		let filterableItems1 = []
		if (pantryOn) {
			filterableItems1 = availableItems(allItems)
		}
		if (!pantryOn) {
			filterableItems1 = allItems
		}

		if (filterableItems1) {

			let categories = filterableItems1.map((el) => {
				return el.category;
			}, []);
			setcatsFilter([...new Set(categories)]);

			let levels = filterableItems1.map((el) => {
				return el.level;
			}, []);
			setlevFilter([...new Set(levels)]);

			let storage = filterableItems1.map((el) => {
				return el.storage;
			}, []);
			setstoreFilter([...new Set(storage)]);

			const tags = filterableItems1.flatMap(({ tags = [] }) => tags);
			settagFilter([...new Set(tags)]);

		}
		setfilterableItems(filterableItems1)

		return function cleanup() {
			console.log('unmounting filter');
		}

	}, [pantryOn, allItems])

	const catsAndLevs = () => {
		setOpen(true);

	}


	const handleClickCat = () => {
		setCategoryOpen(!categoryOpen);
	};

	const handleClickLev = () => {
		setLevelOpen(!levelOpen);
	};

	const handleClickStore = () => {
		setstorageOpen(!levelOpen);
	};

	const handleClickTag = () => {
		settagsOpen(!tagsOpen);
	};

	const toggleDrawer = (event) => {
		if (
			event.type === "keydown" &&
			(event.key === "Tab" || event.key === "Shift")
		) {
			return;
		}
		setOpen(!open);
	};

	const clear = () => {
		setinvisible(false)
		dispatch({ type: "FILTERED_CHECKLIST", payload: { categories: [], storages: [], levels: [], tags: [] } });
		setfilterCategories([])
		setfilterLevel([]);
		setfilterStorage([])
		setfilterTags([])

		setcatChecked([]);
		setlevChecked([]);
		setstoreChecked([])
		settagsChecked([])

	};

	const sendFilters = () => {
		if (filterCheckList.categories.length > 0 ||
			filterCheckList.levels.length > 0 ||
			filterCheckList.storages.length > 0 ||
			filterCheckList.tags.length > 0) {
			setinvisible(false)
			console.log('filter on');
			dispatch({ type: "FILTER_ON" });
		}
		if (!filterCheckList.categories.length > 0 &&
			!filterCheckList.levels.length > 0 &&
			!filterCheckList.storages.length > 0 &&
			!filterCheckList.tags.length > 0) {
			setinvisible(true)
			console.log('filter Off');
			dispatch({ type: "FILTER_OFF" });
		}
		setOpen(!open);
	};

	return (
		<>
			<IconButton onClick={catsAndLevs}>
				<Badge variant="dot" invisible={invisible} color="secondary">
					<FilterListIcon
						className={classes.filterIconColor}
					/>
				</Badge>
			</IconButton>


			<Drawer anchor="right" open={open} onClose={toggleDrawer}>
				<div className={classes.drawerList}>
					<List component="nav" aria-label="secondary mailbox folders">
						<ListItem>
							<ListItemText primary="Filter" />
							<ListItemSecondaryAction>
								<Button variant="contained" disableElevation onClick={clear}>
									Clear
								</Button>
							</ListItemSecondaryAction>
						</ListItem>
						<Divider />
						<ListItem button onClick={handleClickCat}>
							<ListItemText primary={`Categories (${catsFilter.length})`} />

							<ListItemSecondaryAction>
								{filterCategories.length > 0 ?
									<Badge badgeContent={filterCategories.length} color="secondary">
										<CheckIcon fontSize="small" />
									</Badge> :
									<Typography>
										All
									</Typography>
								}
							</ListItemSecondaryAction>

						</ListItem>
						<Divider />
						<ListItem button onClick={handleClickLev}>
							<ListItemText primary={`Levels (${levFilter.length})`} />
							<ListItemSecondaryAction>
								{filterLevel.length > 0 ?
									<Badge badgeContent={filterLevel.length} color="secondary">
										<CheckIcon fontSize="small" />
									</Badge> :
									<Typography>
										All
									</Typography>
								}
							</ListItemSecondaryAction>
						</ListItem>
						<Divider />
						<ListItem button onClick={handleClickStore}>
							<ListItemText primary={`Storage (${storeFilter.length})`} />
							<ListItemSecondaryAction>
								{filterStorage.length > 0 ?
									<Badge badgeContent={filterStorage.length} color="secondary">
										<CheckIcon fontSize="small" />
									</Badge> :
									<Typography>
										All
									</Typography>
								}
							</ListItemSecondaryAction>
						</ListItem>
						<Divider />
						<ListItem button onClick={handleClickTag}>
							<ListItemText primary={`Tags (${tagFilter.length})`} />
							<ListItemSecondaryAction>
								{filterTags.length > 0 ?
									<Badge badgeContent={filterTags.length} color="secondary">
										<CheckIcon fontSize="small" />
									</Badge> :
									<Typography>
										All
									</Typography>
								}
							</ListItemSecondaryAction>
						</ListItem>
						<Divider />
					</List>
					<div className={classes.filterButton}>
						<Button
							variant="contained"
							color="primary"
							size="large"
							fullWidth
							onClick={sendFilters}
						>
							View Items
						</Button>
					</div>

				</div>
			</Drawer>
			<ListDrawerCatergories
				categoryOpen={categoryOpen}
				setCategoryOpen={setCategoryOpen}
				setfilterCategories={setfilterCategories}
				catsFilter={catsFilter}
				catChecked={catChecked}
				setcatChecked={setcatChecked}
				filterableItems={filterableItems}

			/>
			<ListDrawerLevels
				levelOpen={levelOpen}
				setLevelOpen={setLevelOpen}
				setfilterLevel={setfilterLevel}
				levFilter={levFilter}
				levChecked={levChecked}
				setlevChecked={setlevChecked}
				filterableItems={filterableItems}
			/>
			<ListDrawerStorage
				storageOpen={storageOpen}
				setstorageOpen={setstorageOpen}
				setfilterStorage={setfilterStorage}
				storeFilter={storeFilter}
				storeChecked={storeChecked}
				setstoreChecked={setstoreChecked}
				filterableItems={filterableItems}
			/>
			<ListDrawerTags
				tagsOpen={tagsOpen}
				settagsOpen={settagsOpen}
				setfilterTags={setfilterTags}
				tagFilter={tagFilter}
				tagsChecked={tagsChecked}
				settagsChecked={settagsChecked}
				filterableItems={filterableItems}
			/>
		</>
	);
}
