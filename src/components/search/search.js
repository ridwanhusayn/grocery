import React from 'react'
import { AppState } from '../../context/app-context'
import { DebounceInput } from 'react-debounce-input';

import {
    TextField, InputAdornment, Box
} from '@material-ui/core/'
import SearchIcon from '@material-ui/icons/Search';


export default function Search() {
    console.log('search loaded');
    const [
        stateContext, dispatch,
    ] = AppState()
    const { searchWord } = stateContext;


    const handleSearch = (e) => {
        dispatch({ type: "SEARCH", payload: e.target.value });

    };


    return (
        <>
            <Box p={2}>
                <DebounceInput
                    element={TextField}
                    placeholder="Search.."
                    value={searchWord}
                    minLength={2}
                    debounceTimeout={300}
                    onChange={handleSearch}

                    id="outlined-start-adornment"
                    InputProps={{
                        startAdornment:
                            <InputAdornment position="start">
                                <SearchIcon />
                            </InputAdornment>,
                    }}
                    variant="outlined"
                    type="search"
                    size="small"
                    fullWidth
                />
            </Box>
        </>

    )
}

