import React, { Fragment, useState } from 'react'
import { AppState } from '../../context/app-context'
import {
    List, ListItem, ListItemText, Collapse, makeStyles, Divider, ListItemIcon, Box, Typography
} from '@material-ui/core'
import {
    ExpandLess,
    ExpandMore,
} from '@material-ui/icons';
import CategoryIcon from '@material-ui/icons/Category';
import ListAltIcon from '@material-ui/icons/ListAlt';
import FormatListBulletedIcon from '@material-ui/icons/FormatListBulleted';
import StorageIcon from '@material-ui/icons/Storage';
import LocalOfferIcon from '@material-ui/icons/LocalOffer';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';

import MailIcon from '@material-ui/icons/Mail';
import { Link } from 'react-router-dom'
import { userLogout } from '../../utils/actions'


const useStyles = makeStyles((theme) => ({
    nested: {
        paddingLeft: theme.spacing(4),
    },
    topList: {
        background: "linear-gradient(45deg, rgb(33, 150, 243) 30%, rgb(33, 203, 243) 90%)",
        paddingTop: theme.spacing(6),
        paddingBottom: theme.spacing(1),
        color: "#ffffff"

    },

}))

export default function ListsDrawerLeft(props) {

    const classes = useStyles();
    const [
        stateContext, dispatch
    ] = AppState()

    const { allList } = stateContext;

    const [open, setOpen] = useState(false)

    const handleClick = () => {
        setOpen(!open);
    }

    const listToAdd = () => {
        let listArr = []
        if (allList)
            allList.forEach(list => {
                console.log("list >", list);
                listArr.push(
                    <Fragment key={list.id}>

                        <ListItem button className={classes.nested} key={list.id}
                            onClick={props.toggleGroup(list.name)}
                            component={Link} to={{
                                pathname: '/listPage',
                                listId: list.id
                            }}>
                            <ListItemIcon>
                                <FormatListBulletedIcon />
                            </ListItemIcon>
                            <ListItemText primary={list.name} />
                        </ListItem>
                    </Fragment>
                )
            })
        return listArr
    }


    return (
        <>
            <Box p={5} className={classes.topList}>
                <Typography variant="h6">Pantry Mate</Typography>
            </Box>
            <Divider />
            <List>
                <ListItem button onClick={props.togglePamtry(true, 'Pantry')}
                    component={Link} to={{
                        pathname: '/',
                    }}>
                    <ListItemIcon><ListAltIcon /></ListItemIcon>
                    <ListItemText primary="Pantry" />
                </ListItem>

                <ListItem button onClick={props.togglePamtry(false, 'All items')}
                    component={Link} to={{
                        pathname: '/',
                    }}>
                    <ListItemIcon><ListAltIcon /></ListItemIcon>
                    <ListItemText primary="All Items" />
                </ListItem>

                <ListItem onClick={handleClick}>
                    <ListItemIcon>
                        <FormatListBulletedIcon />
                    </ListItemIcon>
                    <ListItemText primary="Lists" />
                    {open ? <ExpandLess /> : <ExpandMore />}
                </ListItem>
                <Collapse in={open} timeout="auto" unmountOnExit>
                    <List component="div" disablePadding >
                        <ListItem button className={classes.nested} onClick={props.toggleGroup('All Lists')}
                            component={Link} to={{
                                pathname: '/listGroups',
                            }}>
                            <ListItemIcon>
                                <FormatListBulletedIcon />
                            </ListItemIcon>
                            <ListItemText primary="All Lists" />
                        </ListItem>

                        {listToAdd()}

                    </List>
                </Collapse>

                <ListItem button onClick={props.toggleGroup('Category')}
                    component={Link} to={{
                        pathname: '/categories',
                    }}>
                    <ListItemIcon><CategoryIcon /></ListItemIcon>
                    <ListItemText primary="Categories" />
                </ListItem>

                <ListItem button onClick={props.toggleGroup('Storage')}
                    component={Link} to={{
                        pathname: '/storage',
                    }}>
                    <ListItemIcon><StorageIcon /></ListItemIcon>
                    <ListItemText primary="Storages" />
                </ListItem>

                <ListItem button onClick={props.toggleGroup('Tags')}
                    component={Link} to={{
                        pathname: '/tags',
                    }}>
                    <ListItemIcon><LocalOfferIcon /></ListItemIcon>
                    <ListItemText primary="Tags" />
                </ListItem>
                <Divider />
                <ListItem button onClick={userLogout}>
                    <ListItemIcon><ExitToAppIcon /></ListItemIcon>
                    <ListItemText primary="Log Out" />
                </ListItem>
            </List>
        </>
    )
}
