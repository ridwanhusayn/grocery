import React from 'react'
import {
    Grid,
    Typography
} from "@material-ui/core";
import SentimentVeryDissatisfiedIcon from '@material-ui/icons/SentimentVeryDissatisfied';

export default function NoItemsFound() {
    return (
        <div>
            <Grid
                container
                spacing={0}
                direction="column"
                alignItems="center"
                justify="center"
                style={{ minHeight: '40vh' }}
            >
                <Grid item xs={10}>
                    <i> <Typography align="center" variant="h5">No items match your criteria.</Typography> </i>
                </Grid>
                <Grid item xs={10}>
                    <SentimentVeryDissatisfiedIcon fontSize="large" />
                </Grid>
            </Grid>

        </div>
    )
}
