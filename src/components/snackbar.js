import React from 'react'
import { Snackbar, SnackbarContent, makeStyles } from '@material-ui/core'
import MuiAlert from '@material-ui/lab/Alert';

import { AppState } from '../context/app-context'

function Alert(props) {
    return <MuiAlert variant="filled" {...props} />;
}

const useStyles = makeStyles((theme) => ({

    root: {
        boxShadow: '0 2px 5px -2px rgb(0 0 0 / 20%)',
    },

    snackbar: {
        bottom: 56,
    },
}));

export default function SnackbarComp() {

    const classes = useStyles();

    const [
        stateContext, dispatch

    ] = AppState()

    const { snackbarOpen } = stateContext;


    const handleClose = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }
        dispatch({ type: "SNACKBAR", payload: { open: false, message: null, severity: null } });
    }


    return (

        <Snackbar
            autoHideDuration={6000}
            anchorOrigin={{
                vertical: 'bottom',
                horizontal: 'center',
            }}
            open={snackbarOpen.open}
            onClose={handleClose}
            className={classes.snackbar}
        >

            <Alert onClose={handleClose}
                classes={{
                    root: classes.root
                }}>
                {snackbarOpen.message}
            </Alert>
        </Snackbar>
    )
}
