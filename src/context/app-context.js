import React, { useReducer, useState } from "react";
import { availableItems, filterArray } from '../utils/actions.js';
const StateContext = React.createContext();

const initialState = {
	allItems: [],
	displayItemstoShow: [],
	filterOn: 0,
	pantryOn: true,
	searchWord: undefined,
	allList: [],
	allCategories: [],
	allStorage: [],
	allItemTags: [],
	snackbarOpen: { open: false, message: null, severity: null },
	filterCheckList: { categories: [], storages: [], levels: [], tags: [] },
	page: 'Pantry',
	checked: [],
	checkedList: [],
	uncheckedList: []
}

const reducer = (state, action) => {
	//initialise the App
	switch (action.type) {
		case "INIT": {
			return {
				...state,
				allItems: action.payload,
			}
		}
		case "SEARCH": {
			return {
				...state,
				searchWord: action.payload,
			}
		}
		case "ALL_LIST": {
			return {
				...state,
				allList: action.payload,
			}
		}
		case "ALL_CATEGORIES": {
			return {
				...state,
				allCategories: action.payload,
			}
		}
		case "ALL_STORAGE": {
			return {
				...state,
				allStorage: action.payload,
			}
		}
		case "ALL_TAGS": {
			return {
				...state,
				allItemTags: action.payload,
			}
		}
		case "SNACKBAR": {
			return {
				...state,
				snackbarOpen: action.payload,
			}
		}
		case "DISPLAYITEMS": {
			return {
				...state,
				displayItemstoShow: action.payload,
			}
		}
		case "FILTERED_CHECKLIST": {
			return {
				...state,
				filterCheckList: action.payload,
			}
		}
		case "PAGE": {
			return {
				...state,
				page: action.payload,
			}
		}
		case "CHECKED": {
			return {
				...state,
				checked: action.payload,
			}
		}
		case "CHECKED_LIST": {
			return {
				...state,
				checkedList: action.payload,
			}
		}
		case "UNCHECKED_LIST": {
			return {
				...state,
				uncheckedList: action.payload,
			}
		}
		case "PANTRYON": {
			return {
				...state,
				pantryOn: action.payload,
				filterOn: false
			}
		}
		case "FILTER_ON": {
			return {
				...state,
				filterOn: state.filterOn + 1
			}
		}
		case "FILTER_OFF": {
			return {
				...state,
				filterOn: 0
			}
		}
		default:
			return state;
	}
};

function StateProvider({ children }) {
	const [state, dispatch] = useReducer(reducer, initialState)

	return (
		<StateContext.Provider value={[
			state, dispatch
		]}>
			{children}
		</StateContext.Provider>
	)
}


function AppState() {

	const context = React.useContext(StateContext)
	if (context === undefined) {
		throw new Error('state must be used within Provide')
	}
	return context
}

export { StateProvider, AppState }