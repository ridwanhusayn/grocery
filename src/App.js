import React from 'react'
import { StateProvider } from './context/app-context';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import { AuthProvider } from './utils/auth'
import './App.css'
import PrivateRoute from './utils/privateRoute'
import { MuiPickersUtilsProvider } from '@material-ui/pickers';
import DateFnsUtils from '@date-io/date-fns';

// pages
import home from './pages/home/home'
import groups from './pages/groups/groups'
import listGroups from './pages/listGroup/listGroups'
import listPage from "./pages/checklist/listPage";
// Components
import Login from './pages/login/login'


import { createMuiTheme } from '@material-ui/core/styles';
import { ThemeProvider } from '@material-ui/styles';

const theme = createMuiTheme({
  palette: {
    primary: {
      main: '#2196f3'
    }
  }
});


const App = () => {

  return (
    <StateProvider>
      <AuthProvider>
        <ThemeProvider theme={theme}>
          <MuiPickersUtilsProvider utils={DateFnsUtils}>
            <Router>
              <div>
                <Switch>
                  <PrivateRoute exact path='/' component={home} />
                  <PrivateRoute exact path='/categories' component={groups} />
                  <PrivateRoute exact path='/storage' component={groups} />
                  <PrivateRoute exact path='/tags' component={groups} />
                  <PrivateRoute exact path='/listGroups' component={listGroups} />
                  <PrivateRoute exact path='/listPage' component={listPage} />
                  <Route exact path='/login' component={Login} />
                </Switch>
              </div>
            </Router>
          </MuiPickersUtilsProvider>
        </ThemeProvider>
      </AuthProvider>
    </StateProvider>
  )
}

export default App